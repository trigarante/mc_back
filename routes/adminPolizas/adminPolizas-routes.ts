import {Router, Request, Response} from "express";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import PlazaQueries from "../../queries/RRHH/plazaQueries";
const generales = require('../../general/function');

const adminPolizasRoutes = Router();

adminPolizasRoutes.get('/admin-polizas/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        switch (permisos.idPermisoVisualizacion) {
            case 1:
                adminPolizas = await AdministradorPolizasQueries.getAll(fechaInicio, fechaFin)
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     adminPolizas = await AdministradorPolizasQueries.getAllByHijos(aux, fechaInicio, fechaFin)
                // } else {
                    const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                    let aux = [];
                    for (const departamento of departamentos) {
                        aux.push(departamento.idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(permisos.idDepartamento)
                    }
                    adminPolizas = await AdministradorPolizasQueries.getAllByDepartamentos(aux, fechaInicio, fechaFin)
                // }
                break;
            case 3:
                adminPolizas = await AdministradorPolizasQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})


export default adminPolizasRoutes;
