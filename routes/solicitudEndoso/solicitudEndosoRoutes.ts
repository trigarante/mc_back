import {Request, Response, Router} from "express";
import SolicitudEndosoQueries from "../../queries/solicitudEndoso/solicitudEndosoQueries";
import UsuariosQuery from "../../queries/usuarios/usuariosQuery";
import PermisosQueries from "../../queries/permisos/permisosQueries";
import PagoQueries from "../../queries/verificaciones/pagoQueries";

const generales = require('../../general/function');
const solicitudEndosoRoutes = Router();

solicitudEndosoRoutes.get('/get-endosoPendiente/:idEmpleado/:fechaInicio/:fechaFin', async(req: Request, res: Response, next) => {
    try {
        const {idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!idEmpleado) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await SolicitudEndosoQueries.getPermisosVisualizacion(+idEmpleado);
        let SolicitudEndoso;
        switch (permisos.idPermisoVisualizacion) {
            case 1:
                SolicitudEndoso = await SolicitudEndosoQueries.getAll(fechaInicio, fechaFin)
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PermisosQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         // @ts-ignore
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         // @ts-ignore
                //         aux.push(+idEmpleado)
                //     }
                //     SolicitudEndoso = await SolicitudEndosoQueries.getAllByHijos(fechaInicio, fechaFin, aux)
                // } else {
                    const departamentos = await UsuariosQuery.getDepartamentoByIdUsuario(permisos.idUsuario);
                    const aux = [];
                    for (const departamento of departamentos) {
                        // @ts-ignore
                        aux.push(departamento.idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(permisos.idDepartamento)
                    }
                    SolicitudEndoso = await SolicitudEndosoQueries.getAllByDepartamentos(fechaInicio, fechaFin, aux);
                // }
                break;
            case 3:
                SolicitudEndoso = await SolicitudEndosoQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( SolicitudEndoso );
    } catch (e) {
        res.status(500).send(e)
    }
})

solicitudEndosoRoutes.post('/', async (req: Request, res: Response, next) => {
    try {
        await SolicitudEndosoQueries.post(req.body)
        return res.status(200).send();
    } catch (err) {
        next(err)
    }
});


export default solicitudEndosoRoutes;
