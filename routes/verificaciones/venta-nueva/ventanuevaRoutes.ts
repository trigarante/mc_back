import {Request, Response, Router} from "express";
import VentaNuevaController from "../../../controller/verificaciones/venta-nueva/ventanuevaController";
import generales from "../../../general/function";
import PermisosQueries from "../../../queries/permisos/permisosQueries";
import VentanuevaQueries from "../../../queries/verificaciones/venta-nueva/ventanuevaQueries";
import UsuariosQuery from "../../../queries/usuarios/usuariosQuery";

const router = Router();

router.get('/:idEmpleado', VentaNuevaController.getByEmpleado);
router.get('/get-by-id/:idVerificacion', VentaNuevaController.getByIdVarificacion);
router.get('/tabla/:tipo/:idEmpleado/:idFlujoPoliza/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, idFlujoPoliza, fechaInicio, fechaFin, tipo} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin && idFlujoPoliza)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await PermisosQueries.findPermisos(+idEmpleado);
        let registro;
        let aux2;
        let aux3;
        let aux4;
        if(+idFlujoPoliza === 26) {
            aux3 = '';
            aux4 = '';
        } else  {
            aux3 = 'LEFT JOIN operaciones.inspecciones ON operaciones.inspecciones."idRegistro" = operaciones."verificacionRegistro"."idRegistro"';
            aux4 = `operaciones.inspecciones.archivo "archivoInspeccion",
                operaciones.inspecciones.id "idInspeccion",`;
        }
        // se agregan los casos 5,6,7,8 para los casos de polizas desviadas
        switch (+tipo) {
            case 1:
                aux2 = `("verificadoCliente" = 1 or "verificadoProducto" = 1 or "verificadoRegistro" = 1 or "verificadoPago" = 1) AND "idEstadoVerificacion" <> 3  and registro.desviada = 0`
                break;
            case 2:
                aux2 = `("verificadoCliente" > 1 or "verificadoProducto" > 1 or "verificadoRegistro" > 1 or "verificadoPago" > 1) and "idEstadoVerificacion" = 2  and registro.desviada = 0`
                break;
            case 3:
                aux2 = `("verificadoCliente" = 3 or "verificadoProducto" = 3 or "verificadoRegistro" = 3 or "verificadoPago" = 3) AND "idEstadoVerificacion" = 2  and registro.desviada = 0`
                break;
            case 4:
                aux2 = `"idEstadoVerificacion" = 3  and registro.desviada = 0`
                break;
            case 5:
                aux2 = `("verificadoCliente" = 1 or "verificadoProducto" = 1 or "verificadoRegistro" = 1 or "verificadoPago" = 1) AND "idEstadoVerificacion" <> 3  and registro.desviada IN (1, 2)`
                break;
            case 6:
                aux2 = `("verificadoCliente" > 1 or "verificadoProducto" > 1 or "verificadoRegistro" > 1 or "verificadoPago" > 1) and "idEstadoVerificacion" = 2  and registro.desviada IN (1, 2)`
                break;
            case 7:
                aux2 = `("verificadoCliente" = 3 or "verificadoProducto" = 3 or "verificadoRegistro" = 3 or "verificadoPago" = 3) AND "idEstadoVerificacion" = 2  and registro.desviada IN (1, 2)`
                break;
            case 8:
                aux2 = `"idEstadoVerificacion" = 3  and registro.desviada IN (1, 2)`
                break;
        }
        console.log('----aux2---', aux2)
        if (+tipo !==  1 && permisos.idPermisoVisualizacion === 3) {
            aux2 += ` AND "verificacionRegistro"."idEmpleado" = ${idEmpleado}`
        }
        switch (+permisos.idPermisoVisualizacion) {
            case 1:
                registro = await VentanuevaQueries.findAll(fechaInicio, fechaFin, idFlujoPoliza, aux2, aux3, aux4);
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PermisosQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         // @ts-ignore
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         // @ts-ignore
                //         aux.push(+idEmpleado)
                //     }
                //     registro = await VentanuevaQueries.findAllByHijos(fechaInicio, fechaFin, idFlujoPoliza, aux, aux2, aux3, aux4)
                // } else {
                const departamentos = await UsuariosQuery.findAllPoliza(permisos.idUsuario);
                const aux = [];
                for (const departamento of departamentos) {
                    // @ts-ignore
                    aux.push(departamento.idDepartamento)
                }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                registro = await VentanuevaQueries.findAllByDepartamentos(fechaInicio, fechaFin, idFlujoPoliza, aux, aux2, aux3, aux4);
                // }
                break;
            case 3:
                registro = await VentanuevaQueries.findAllByDepartamentoAndIdEmpleado(fechaInicio, fechaFin, idFlujoPoliza, permisos.idDepartamento, idEmpleado, aux2);
                break;

        }
        return  res.json(registro);
    } catch (e) {
        return res.status(500).json({
            ok: false,
            msg: 'error revisar consola del back'
        });
    }
});
// router.get('/empleado/:estadoVerificacion/:idEmpleado/:idFlujoPoliza', VentaNuevaController.getTablaConEmpleado);
router.get('/tabla-renovaciones/:estadoVerificacion/:idEmpleado/:idFlujoPoliza', VentaNuevaController.getTablaRenovaciones);
router.post('', VentaNuevaController.postVerificacion);
router.put('/:id', VentaNuevaController.putVerificacion);
router.put('/:idVerificacionRegistro/:idDocumentoVerificado/:estado', VentaNuevaController.putDocumentoVerificado);

// Liga para buscar poliza
router.get('/tabla/:tipo/:idEmpleado/:idFlujoPoliza/:poliza', async (req: Request, res: Response,)=>{
    try {
        const {idFlujoPoliza, poliza, tipo, idEmpleado} = req.params;
        const permiso = await PermisosQueries.findPermisos(+idEmpleado)

        if (!(poliza)) return generales.manejoErrores('No se envio el id empleado', res);

        let adminPolizas;
        let aux2;
        let aux3;
        let aux4;
        if(+idFlujoPoliza === 26) {
            aux3 = '';
            aux4 = '';
        } else  {
            aux3 = 'LEFT JOIN operaciones.inspecciones ON operaciones.inspecciones."idRegistro" = operaciones."verificacionRegistro"."idRegistro"';
            aux4 = `operaciones.inspecciones.archivo "archivoInspeccion",
                operaciones.inspecciones.id "idInspeccion",`;
        }
        switch (+tipo) {
            case 1:
                aux2 = `("verificadoCliente" = 1 or "verificadoProducto" = 1 or "verificadoRegistro" = 1 or "verificadoPago" = 1) AND "idEstadoVerificacion" <> 3 and registro.desviada = 0`
                break;
            case 2:
                aux2 = `("verificadoCliente" > 1 or "verificadoProducto" > 1 or "verificadoRegistro" > 1 or "verificadoPago" > 1) and "idEstadoVerificacion" = 2 and registro.desviada = 0`
                break;
            case 3:
                aux2 = `("verificadoCliente" = 3 or "verificadoProducto" = 3 or "verificadoRegistro" = 3 or "verificadoPago" = 3) AND "idEstadoVerificacion" = 2 and registro.desviada = 0`
                break;
            case 4:
                aux2 = `"idEstadoVerificacion" = 3 and registro.desviada = 0`
                break;
            case 5:
                aux2 = `("verificadoCliente" = 1 or "verificadoProducto" = 1 or "verificadoRegistro" = 1 or "verificadoPago" = 1) AND "idEstadoVerificacion" <> 3 and registro.desviada = 1`
                break;
            case 6:
                aux2 = `("verificadoCliente" > 1 or "verificadoProducto" > 1 or "verificadoRegistro" > 1 or "verificadoPago" > 1) and "idEstadoVerificacion" = 2 and registro.desviada = 1`
                break;
            case 7:
                aux2 = `("verificadoCliente" = 3 or "verificadoProducto" = 3 or "verificadoRegistro" = 3 or "verificadoPago" = 3) AND "idEstadoVerificacion" = 2 and registro.desviada = 1`
                break;
            case 8:
                aux2 = `"idEstadoVerificacion" = 3 and registro.desviada = 1`
                break;
        }
        console.log(aux2)

        if (+tipo !==  1 && permiso.idPermisoVisualizacion === 3) {
            aux2 += ` AND "verificacionRegistro"."idEmpleado" = ${idEmpleado}`
        }
        switch (permiso.idPermisoVisualizacion){
            case 1:
                adminPolizas = await VentanuevaQueries.findAllPoliza(poliza, idFlujoPoliza, aux2, aux3, aux4)
                break;
            case 3:
                break;
        }
        adminPolizas = await VentanuevaQueries.findAllPoliza(poliza, idFlujoPoliza, aux2, aux3, aux4)

        res.status(200).send( adminPolizas );
    }catch (e){
        res.status(500).send(e)
    }
})

// liga buscar numero de serie
router.get('/tablaserie/:tipo/:idEmpleado/:idFlujoPoliza/:numero', async (req: Request, resp: Response,)=>{
    try {
        const {numero, idFlujoPoliza, tipo, idEmpleado} = req.params;
        if (!(numero)) return generales.manejoErrores('No se envio el id empleado', resp);
        const permiso = await PermisosQueries.findPermisos(+idEmpleado)
        console.log('esto es el permiso',permiso)
        let adminPolizas;
        let aux2;
        let aux3;
        let aux4;
        if(+idFlujoPoliza === 26) {
            aux3 = '';
            aux4 = '';
        } else  {
            aux3 = 'LEFT JOIN operaciones.inspecciones ON operaciones.inspecciones."idRegistro" = operaciones."verificacionRegistro"."idRegistro"';
            aux4 = `operaciones.inspecciones.archivo "archivoInspeccion",
                operaciones.inspecciones.id "idInspeccion",`;
        }
        switch (+tipo) {
            case 1:
                aux2 = `("verificadoCliente" = 1 or "verificadoProducto" = 1 or "verificadoRegistro" = 1 or "verificadoPago" = 1) AND "idEstadoVerificacion" <> 3`
                break;
            case 2:
                aux2 = `("verificadoCliente" > 1 or "verificadoProducto" > 1 or "verificadoRegistro" > 1 or "verificadoPago" > 1) and "idEstadoVerificacion" = 2`
                break;
            case 3:
                aux2 = `("verificadoCliente" = 3 or "verificadoProducto" = 3 or "verificadoRegistro" = 3 or "verificadoPago" = 3) AND "idEstadoVerificacion" = 2`
                break;
            default:
                aux2 = `"idEstadoVerificacion" = 3`
                break;
        }
        if (+tipo !==  1 && permiso.idPermisoVisualizacion === 3) {
            aux2 += ` AND "verificacionRegistro"."idEmpleado" = ${idEmpleado}`
        }
        switch (permiso.idPermisoVisualizacion){
            case 1:
                adminPolizas = await VentanuevaQueries.getAllBySerie(numero, idFlujoPoliza,aux2, aux3, aux4)
                break;
            case 3:
                break;
        }

        console.log('esto es aux 2', aux2)

        adminPolizas = await VentanuevaQueries.getAllBySerie(numero, idFlujoPoliza,aux2, aux3, aux4)
        resp.status(200).send( adminPolizas );
    } catch (e) {
        resp.status(500).send(e)
    }
})

export default router;
