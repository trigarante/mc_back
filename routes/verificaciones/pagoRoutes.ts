import {Router} from "express";
import PagoController from "../../controller/verificaciones/pagoController";

const router = Router();

router.get('/:tipo/:idEmpleado/:fechaInicio/:fechaFin', PagoController.getAllByIdEmpleado);
router.get('/:id', PagoController.getById);
router.put('/', PagoController.updatePago);
router.put('/concluir-autorizacion/:idVerificacionPago', PagoController.activateVerificado);

export default router;
