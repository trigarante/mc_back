import {Router} from "express";
import ErroresSubsecuentesAnexosController from "../../controller/errores-anexos/ErroresSubsecuentesAnexosVerificacionController";

const router = Router();

router.get('/:idVerificacionPago', ErroresSubsecuentesAnexosController.getPCorregir);
router.post('', ErroresSubsecuentesAnexosController.createErroresAVP);
router.put('/:idErrorAnexoPago', ErroresSubsecuentesAnexosController.updateEstadoCorreccion);


export default router;
