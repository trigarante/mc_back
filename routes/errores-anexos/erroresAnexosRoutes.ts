import {Request, Response, Router} from "express";
import ErroresAnexosController from "../../controller/errores-anexos/erroresAnexosCotroller";
import ErroresAnexosV from "../../entity/errores-anexsos/erroresAnexosVEntity";
import CatalogoErroresDocsQueries from "../../queries/verificaciones/catalogoErroresDocsQueries";
import PenalizacionesAV from "../../entity/penalizaciones/penalizacionesAnezosVEntity";
import VerificacionRegistro from "../../entity/verificaciones/verificacionRegistroEntity";
import PagoQueries from "../../queries/verificaciones/pagoQueries";
import Pagos from "../../entity/pagos/pagoEntity";
import generales from "../../general/function";
import {where} from "sequelize";

const router = Router();

router.get('/', ErroresAnexosController.getAll);
router.get('/:idVerificacionRegistro/:idTipoDocumento', ErroresAnexosController.getByIdVerificaiconRegistro);
router.get('/get-comentarios/:idVerificacionRegistro/:idTipoDocumento', async(req: Request, res: Response) => {
    try {
        const {idVerificacionRegistro, idTipoDocumento} = req.params;
        const erroresAnexsos = await ErroresAnexosV.findAll({
            where: {idVerificacionRegistro, idTipoDocumento, idEstadoCorreccion: 2}
        });
        return res.json(erroresAnexsos);
    } catch (e) {
        return res.status(500).json({
            ok: false,
            msg: 'error revisar consola del back'
        });
    }
});
router.get('/get-comentarios-historico/:idVerificacionRegistro/:idTipoDocumento', async(req: Request, res: Response) => {
    try {
        const {idVerificacionRegistro, idTipoDocumento} = req.params;
        if (!(idVerificacionRegistro && idTipoDocumento)) return generales.manejoErrores('No se envió los id', res);
        const erroresAnexsos = await ErroresAnexosV.findAll({
            where: {idVerificacionRegistro, idTipoDocumento}
        });
        return res.json(erroresAnexsos);
    } catch (e) {
        return res.status(500).json({
            ok: false,
            msg: 'error revisar consola del back'
        });
    }
});

router.post('/', async(req: Request, res: Response) => {
    console.log(req.body)
    const erroresV = await ErroresAnexosV.create(req.body).then(data => { return data})
    console.log(erroresV)
    const erroresAnexos = req.body;
    for(const correccion of erroresAnexos.correcciones) {
        const penalizable = correccion.penalizable;
        const penalizacionesPCampo: any = await ErroresAnexosController.getVerificacion({
            idVerificacionRegistro: req.body.idVerificacionRegistro,
            idCatalogoErroresDocumentos: correccion.idCatalogoErroresDocumentos,
        });
        console.log(penalizacionesPCampo)
        console.log(penalizable)
        if(penalizacionesPCampo.length === 0 && penalizable) {
            const penalizacion = await CatalogoErroresDocsQueries.findAllCatalogo(correccion.idCatalogoErroresDocumentos);
            console.log(penalizacion)
            const arreglo = {
                idVerificacionRegistro: erroresAnexos.idVerificacionRegistro,
                idCatalogoErroresDocumentos: penalizacion.id,
                montoPenalizacionEjecutivo: penalizacion.penalizacionEjecutivo,
                montoPenalizacionSupervisor: penalizacion.penalizacionSupervisor
            };
            await PenalizacionesAV.create(arreglo)
        }
    }
    const verificacionRegistro:any = await VerificacionRegistro.findByPk(erroresAnexos.idVerificacionRegistro, {raw: true});
    console.log(verificacionRegistro)
    if (erroresAnexos.idTipoDocumento===5 && verificacionRegistro.verificadoPago === 2){
        console.log('*********************************************si')
        const idPago = await PagoQueries.findByIdVerificacion(erroresAnexos.idVerificacionRegistro);
        await PagoQueries.updateEdoPago({idEstadoPago: 2}, idPago).catch(e => console.log(e))
    }
    // const vRegistro = {
    //     id: verificacionRegistro?.getDataValue("id"),
    //     idRegistro: verificacionRegistro?.getDataValue("idRegistro"),
    //     idEstadoVerificacion: verificacionRegistro?.getDataValue("idEstadoVerificacion"),
    //     idEmpleado: verificacionRegistro?.getDataValue("idEmpleado"),
    //     numero: verificacionRegistro?.getDataValue("numero"),
    //     verificadoCliente: verificacionRegistro?.getDataValue("verificadoCliente"),
    //     verificadoProducto: verificacionRegistro?.getDataValue("verificadoProducto"),
    //     verificadoRegistro: verificacionRegistro?.getDataValue("verificadoRegistro"),
    //     verificadoPago: verificacionRegistro?.getDataValue("verificadoPago"),
    //     verificadoInspeccion: verificacionRegistro?.getDataValue("verificadoInspeccion"),
    //     fechaCreacion: verificacionRegistro?.getDataValue("fechaCreacion"),
    //     fechaInicio: verificacionRegistro?.getDataValue("fechaInicio"),
    //     fechaConclusion: verificacionRegistro?.getDataValue("fechaConclusion"),
    // }
    // console.log(erroresAnexos)
    // switch (erroresAnexos.idTipoDocumento) {
    //     case 1: vRegistro.verificadoCliente = 2;
    //         break;
    //     case 2: vRegistro.verificadoRegistro = 2;
    //         break;
    //     case 3: vRegistro.verificadoProducto = 2;
    //         break;
    //     case 4:
    //         vRegistro.verificadoPago = 2;
    //         const idPago = await PagoQueries.findByIdVerificacion(erroresAnexos.idVerificacionRegistro);
    //         const pago = await Pagos.findByPk(idPago.idPago, {raw: true});
    //         await pago?.update({
    //             idEstadoPago: 2
    //         });
    //     break;
    // }
    // vRegistro.numero += 1;
    // if(vRegistro.idEstadoVerificacion === 1) {
    //     vRegistro.idEstadoVerificacion = 2;
    // }
    // await verificacionRegistro?.update(vRegistro);
    // const verificacionRegistro = await VerificacionRegistro.findByPk(erroresAnexos.idVerificacionRegistro, {raw: true});
    console.log(verificacionRegistro)
    let json = {};
    switch (erroresAnexos.idTipoDocumento) {
        case 1: json['verificadoCliente'] = 2;
            break;
        case 2: json['verificadoRegistro'] = 2;
            break;
        case 3: json['verificadoProducto'] = 2;
            break;
        case 4:
            json['verificadoPago'] = 2;
            const idPago = await PagoQueries.findByIdVerificacion(erroresAnexos.idVerificacionRegistro);
            await Pagos.update({idEstadoPago: 2},{
                where: {id: idPago.idPago}
            })
            break;
    }
    json['numero'] = verificacionRegistro.numero + 1;
    if(verificacionRegistro.idEstadoVerificacion === 1) {
        json['idEstadoVerificacion'] = 2;
    }
    await VerificacionRegistro.update(json, {where: {id: erroresAnexos.idVerificacionRegistro}});
    return res.json(erroresV);
});

router.put('/cancelar-correccion/:idVerificacionRegistro/:idTipoDocumento', ErroresAnexosController.cancelarCorreccion);
router.put('/update-estado/:idErroresAnexosV', ErroresAnexosController.updateEstadoCorreccion);


export default router;
