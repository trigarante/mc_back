import {Router} from "express";
import ErroresSubsecuentesVController from "../../controller/errores-anexos/ErroresSubsecuentesVerificacionController";

const router = Router();

router.get('/:idVerificacionPago', ErroresSubsecuentesVController.getPCorregir);
router.post('/', ErroresSubsecuentesVController.createErroresVP);
router.put('/:idErrorAnexoPago', ErroresSubsecuentesVController.updateEstadoCorreccion);


export default router;
