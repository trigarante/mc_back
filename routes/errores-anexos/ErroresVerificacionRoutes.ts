import {Request, Response, Router} from "express";
import ErroresVerificacionController from "../../controller/errores-anexos/ErroresVerificacionController";
import ErroresVerificacion from "../../entity/errores-anexsos/erroresVerificacionEntity";

const router = Router();

router.get('/', ErroresVerificacionController.getAll);
router.get('/:idVerificacionRegistro/:idTipoDocumento', ErroresVerificacionController.getErrores);
router.get('/get-comentarios/:idVerificacionRegistro/:idTipoDocumento', ErroresVerificacionController.getHistoricoErrores);
router.get('/get-comentarios-historico/:idVerificacionRegistro/:idTipoDocumento', async (req: Request, res: Response) => {
    try {
        const {idVerificacionRegistro, idTipoDocumento} = req.params;
        const erroresVerificacion = await ErroresVerificacion.findAll({
            where:{idVerificacionRegistro, idTipoDocumento}
        });
        return res.json(erroresVerificacion);
    } catch (e) {
        return res.status(500).json({
            ok: false,
            msg: 'error revisar consola del back'
        });
    }
});
router.post('', ErroresVerificacionController.createErroresVerifiacion);
router.put('/cancelar-correccion/:idVerificacionRegistro/:idTipoDocumento', ErroresVerificacionController.cancelarCorreccion);
router.put('/:idErrorVerificacion', ErroresVerificacionController.updateEstadoCorreccion);


export default router;
