import {Request, Response, Router} from "express";
import DesviacionService from "../../queries/desviacion/desviacionService";

const DesviacionRoutes = Router();
const generales = require('../../general/function');

DesviacionRoutes.put('/rechazar', async(req:Request, res: Response, next) => {
    try {
        console.log(req.body)
        const {idPago, ...data} = req.body;
        if (!(idPago)) return generales.manejoErrores('No se envio la informacion', res);
        const datos = data.data
        console.log(datos)
        await DesviacionService.update({idEstadoPago: datos.idEstadoPago}, idPago)
        await DesviacionService.postLog(datos)
        await DesviacionService.updateRegistro({idTipificacionDesviacion: datos.idTipificacionDesviacion}, datos.idRegistro)
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});

DesviacionRoutes.get('/info-desviacion', async(req:Request, res: Response, next) => {
    try {
        const {idregistro} = req.headers;
        if (!(idregistro)) return generales.manejoErrores('No se envio la informacion', res);
        const registro: any = await DesviacionService.tipoDesviacion(idregistro)
        console.log(registro)
        const desviacion: any = await DesviacionService.deviacionByIdRegistro(registro.idDesviacion)
        const departamentos = await DesviacionService.departamentosAndSociosByIdDesviacion(registro.idDesviacion)

        res.status(200).send({desviacion, departamentos})
    } catch (e) {
        res.sendStatus(500);
    }
});

DesviacionRoutes.get('/tipo-desviacion', async(req:Request, res: Response, next) => {
    try {
        const {idregistro} = req.headers;
        if (!(idregistro)) return generales.manejoErrores('No se envio la informacion', res);
        // Get desviacion manual o de un periodo
        const tipoDesviacion: any = await DesviacionService.tipoDesviacion(idregistro)
        console.log('--------------')
        console.log(tipoDesviacion.desviada);
        const tipo = tipoDesviacion.desviada;
        return res.status(200).send( tipo.toString() )
    } catch (e) {
        res.sendStatus(500);
    }
});
export default DesviacionRoutes;
