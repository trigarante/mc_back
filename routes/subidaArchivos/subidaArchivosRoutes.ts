import {Request, Response, Router} from "express";
import DriveApiService from "../../queries/drive/driveApiServices";
const generales = require('../../general/function');

const SubidaArchivosRoutes = Router();

SubidaArchivosRoutes.get('/archivo-bajar', async (req: Request, res: Response, next) => {
    try {
        console.log(req.headers)
        const {archivoid, tipo} = req.headers;
        if (!(archivoid && tipo)) return generales.manejoErrores('No se envió el id de la carpeta', res);
        let file;
        let folder;
        switch (+tipo) {
            case 1:
                folder = await DriveApiService.getIdOfMostRecentFile(archivoid, 'cliente');
                file = await DriveApiService.obtenerArchivo(folder);
                break;
            case 2:
            case 3:
                folder = await DriveApiService.getIdOfMostRecentFile(archivoid, 'poliza');
                file = await DriveApiService.obtenerArchivo(folder);
                break;
            case 4:
            case 5:
                file = await DriveApiService.obtenerArchivo(archivoid);
                break;
        }
        return res.status(200).send([file]);
    } catch (err) {
        next(err)
    }
});

SubidaArchivosRoutes.post('/:poliza', async (req: Request, res: Response, next) => {
    try {
        console.log(req['files'])
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        const {poliza} = req.params;
        const files = req['files'].file;
        if (!poliza) return generales.manejoErrores('No se envió la póliza', res);
        const idFolder = await DriveApiService.findOrCreateFolder(3, poliza);
        await DriveApiService.subirArchivo(idFolder, files)
        return res.status(200).send();
    } catch (err) {
        next(err)
    }
});


export default SubidaArchivosRoutes;
