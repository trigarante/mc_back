import {Router} from 'express';
import LiberacinoCorregidasController from "../../controller/liberacion-corregidas/liberacionCorregidasController";

const router = Router();

router.post('', LiberacinoCorregidasController.postLiberacionCorregidas);

export default router;
