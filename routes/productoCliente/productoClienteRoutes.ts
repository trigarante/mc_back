import {Request, Response, Router} from "express";
import ProductoClienteQueries from "../../queries/productoCliente/productoClienteQueries";

const generales = require('../../general/function');


const productoClienteRoutes = Router();

productoClienteRoutes.get('/getById', async (req: Request, res: Response, next) => {
    const {idproductocliente} = req.headers;
    if(!idproductocliente)
        return generales.manejoErrores('No se envio idProductoCliente', res)
    const productoCliente = await ProductoClienteQueries.getById(idproductocliente);
    res.status(200).send( productoCliente );
});
export default productoClienteRoutes;
