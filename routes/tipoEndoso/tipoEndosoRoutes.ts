import {Request, Response, Router} from "express";
import TipoEndosoQueries from "../../queries/tipoEndoso/tipoEndosoQueries";

const tipoEndosoRoutes = Router();

tipoEndosoRoutes.get('/', async(req: Request, res: Response, next) => {
    try {
        const tipoEndoso = await TipoEndosoQueries.getAll();
        res.status(200).send( tipoEndoso );
    } catch (e) {
        res.status(500).send();
    }
})


export default tipoEndosoRoutes;
