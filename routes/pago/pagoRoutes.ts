import {Request, Response, Router} from "express";
import PagosQueries from "../../queries/pagos/pagoQueries";

const PagoRoutes = Router();
const generales = require('../../general/function');


PagoRoutes.get('/byId/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const byId = await PagosQueries.getById(id);
        res.status(200).send(byId);
    } catch (err) {
        next(err);
    }
});
PagoRoutes.put('/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!(id)) return generales.manejoErrores('No se envio la informacion', res);
        await PagosQueries.update(req.body, id)
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});
PagoRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        await PagosQueries.post(req.body)
        return res.status(200).send();
        // return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

export default PagoRoutes;
