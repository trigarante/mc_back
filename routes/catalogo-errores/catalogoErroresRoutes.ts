import {Router} from 'express';
import CatalogoErroresController from "../../controller/catalogo-errores/catalogoErroresController";

const router = Router();

router.get('', CatalogoErroresController.getAllCatalogoErrores);
router.get('/:id', CatalogoErroresController.getAllCatalogoErroresById);
router.get('/campo-corregir/:idCampoCorregir', CatalogoErroresController.getCatalogoErroresCampoCorregir);
router.post('', CatalogoErroresController.postCatalogoErrores);
router.put('', CatalogoErroresController.putCatalogoErrores);

export default router;
