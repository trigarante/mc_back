import {Request, Response, Router} from "express";
const DescargasRoutes = Router();

// Dashboard's routes
DescargasRoutes.get('/avance-ejecutivos/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/llamadas/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/avance/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/pnc/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/llamadas-reno-entrada/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/llamadas-reno-salida/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});

export default DescargasRoutes;
