import {Router} from 'express';
import CampoCorregirController from "../../controller/campo-corregir/campoCorregirController";

const router = Router();

router.get('/forMoral/:idTabla', CampoCorregirController.getCampoCorregirByIdTablaMoral)
router.get('/:idTabla', CampoCorregirController.getCampoCorregirByIdTabla)

export default router;
