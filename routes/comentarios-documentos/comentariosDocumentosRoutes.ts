import {Router} from 'express';
import ComentariosDocumentosController from "../../controller/comentarios-documentos/comentariosDocumentosController";

const router = Router();

router.get('', ComentariosDocumentosController.getAllComentariosDocumentos);
router.get('/:id', ComentariosDocumentosController.getAllComentariosDocumentosById);
router.get('/tipo-documento/:idTipoDocumento', ComentariosDocumentosController.getCatalogoErroresCampoCorregir);
router.post('', ComentariosDocumentosController.postCatalogoErrores);
router.put('', ComentariosDocumentosController.putCatalogoErrores);

export default router;
