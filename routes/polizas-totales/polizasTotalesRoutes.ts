import {Router} from 'express';
import PolizasTotalesController from "../../controller/polizas-totales/polizasTotalesController";

const router = Router();

router.get('/polizas', PolizasTotalesController.getPoliza)

export default router;
