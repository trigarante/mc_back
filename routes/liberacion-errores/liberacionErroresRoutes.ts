import {Router} from 'express';
import LiberacinoErroresController from "../../controller/liberacion-errores/liberacionErroresController";

const router = Router();

router.post('', LiberacinoErroresController.postLiberacionErrores);

export default router;
