import {Router} from 'express';
import TipoDocumentoController from "../../controller/tipo-documento/tipoDocumentoController";

const router = Router();

router.get('/:idTabla', TipoDocumentoController.getTipoDocumentoByIdTabla)

export default router;
