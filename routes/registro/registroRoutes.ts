import {Request, Response, Router} from "express";
import RegistroQueries from "../../queries/registro/registroQueries";

const generales = require('../../general/function');

const registroRoutes = Router();

registroRoutes.get('/viewer/:id', async(req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registro = await RegistroQueries.getForViewer(id);
        res.status(200).send( registro );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.get('/:id', async(req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registro = await RegistroQueries.findAllByIdRegistro(id);
        res.status(200).send( registro );
    } catch (e) {
        res.status(500).send(e)
    }
})
export default registroRoutes;
