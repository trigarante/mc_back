import {Request, Response, Router} from "express";
import PermisosQueries from "../../queries/permisos/permisosQueries";
import AplicacionesViewQueries from "../../queries/aplicacionesView/aplicacionesView";
import UsuariosQuery from "../../queries/usuarios/usuariosQuery";
import VentanuevaQueries from "../../queries/verificaciones/venta-nueva/ventanuevaQueries";
const generales = require("../../general/function")

const router = Router();

router.get('/tabla/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await PermisosQueries.findPermisos(+idEmpleado);
        let registro;
        switch (+permisos.idPermisoVisualizacion) {
            case 1:
                registro = await AplicacionesViewQueries.findAll(fechaInicio, fechaFin);
                break;
            case 2:
                    const departamentos = await UsuariosQuery.getDepartamentoByIdUsuario(permisos.idUsuario);
                    const aux = [];
                    for (const departamento of departamentos) {
                        // @ts-ignore
                        aux.push(departamento.idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(permisos.idDepartamento)
                    }
                    registro = await AplicacionesViewQueries.findAllByDepartamentos(fechaInicio, fechaFin, aux);
                break;
            case 3:
                registro = await AplicacionesViewQueries.findAllByDepartamentoAndIdEmpleado(fechaInicio, fechaFin, permisos.idDepartamento, idEmpleado);
                break;
        }
        return  res.json(registro);
    } catch (e) {
        next(e);
    }
});

router.get('/getById/:idRegistro', async (req: Request, res: Response, next) => {
    try {
        const {idRegistro} = req.params;
        if (!(idRegistro)) return generales.manejoErrores('No se envio el id empleado', res);
        const registro = await AplicacionesViewQueries.findByidRegistro(idRegistro);
        return  res.json(registro);
    } catch (e) {
        return res.status(500).json({
            ok: false,
            msg: 'error revisar consola del back',
            e
        });
    }
});
/**
 * servicio para filtar polizas por poliza, serie o nombre en el admin de polizas
 */
router.get('/datos', async (req: Request, res: Response, next) => {
    try {
        const {numero, fechainicio, fechafin, poliza, serie, nombre} = req.headers;
        if (!numero) return generales.manejoErrores('No se envió el número', res);
        let query;
        switch (+numero) {
            case 1:
                if(!(fechainicio && fechafin)) return generales.manejoErrores('No se envió la info necesaria', res);
                query = `and r."fechaRegistro" BETWEEN date('${fechainicio}') AND date('${fechafin}')`;
                break;
            case 2:
                if(!(poliza)) return generales.manejoErrores('No se envió la poliza', res);
                query = `and r.poliza= '${poliza}'`;
                break;
            case 3:
                if(!(serie)) return generales.manejoErrores('No se envió el numero de serie', res);
                query = `AND pc."noSerie"= '${serie}'`;
                break;
            case 4:
                if(!(nombre)) return generales.manejoErrores('No se envió el nombre', res);
                query = `AND cl.nombre || ' ' || cl."paterno" || ' '  || cl.materno LIKE '%${nombre}%'`
                break;
        }
        const datos = await AplicacionesViewQueries.getDatos(query);
        res.status(datos ? 200 : 500).send(datos);
    }catch (e){
        next(e);
    }
});
router.get('/descargaPNC', async (req: Request, res: Response, next) => {
    try {
        const {fechainicio, fechafin} = req.headers;
        if (!(fechainicio && fechafin)) return generales.manejoErrores('No se enviarón las fechas', res);
        const datos = await AplicacionesViewQueries.descargaPNC(fechainicio, fechafin);
        res.send(datos);
    }catch (e){
        next(e);
    }
});

export default router;
