import { Request, Response } from "express";
import Cotizaciones from "../../../entity/verificaciones/venta-nueva/ventanuevaEntity"
import VentanuevaQueries from "../../../queries/verificaciones/venta-nueva/ventanuevaQueries";
import PermisosQueries from "../../../queries/permisos/permisosQueries";
import UsuariosQuery from "../../../queries/usuarios/usuariosQuery";
import VerificacionRegistro from "../../../entity/verificaciones/verificacionRegistroEntity";
import RegistroEntity from "../../../entity/registro/registroEntity";
import PagoQueries from "../../../queries/verificaciones/pagoQueries";
const generales = require('../../../general/function');

class VentaNuevaController {
    static getByEmpleado = async (req: Request, res: Response) => {
        try {
            const idEmpleado = req.params.idEmpleado;
            const fechaInicio = null, fechaFin = null, idFlujoPoliza = null, aux2 = null, aux3 = null, aux4 = null;
            const registro = await VentanuevaQueries.findAll(fechaInicio, fechaFin, idFlujoPoliza, aux2, aux3, aux4);
            return res.json(registro.filter(registro => registro.idEmpleadoRegistro == idEmpleado));
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getByIdVarificacion = async (req: Request, res: Response) => {
        try {
            const idVerificacion = req.params.idVerificacion;
            if (!idVerificacion) return generales.manejoErrores('No se envio el id', res);
            const verificacion = await VentanuevaQueries.getById(idVerificacion);
            return res.json(verificacion);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static putVerificacion = async (req: Request, res: Response) => {
        try {
            const id = +req.params.id;
            if (!id) return generales.manejoErrores(' No se envio el id', res);
            await VentanuevaQueries.update(req.body, id)
            return res.json();
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static postVerificacion = async (req: Request, res: Response) => {
        try {
            return res.json(true);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static putDocumentoVerificado = async (req: Request, res: Response) => {
        try {
            const { idVerificacionRegistro, idDocumentoVerificado, estado } = req.params;
            console.log(idDocumentoVerificado)
            if (!(idVerificacionRegistro && idDocumentoVerificado && estado)) return generales.manejoErrores('No se enviaron los parametros requeridos', res);
            let autorizacion: any = await VerificacionRegistro.findOne({where: {id: +idVerificacionRegistro}, raw: true})
            switch (+idDocumentoVerificado) {
                case 1:
                    autorizacion.verificadoCliente = +estado
                    break;
                case 2:
                    autorizacion.verificadoRegistro = +estado
                    break;
                case 3:
                    autorizacion.verificadoProducto = +estado
                    break;
                case 4:
                    autorizacion.verificadoPago = +estado
                    break
            }
            console.log(autorizacion)
            if (autorizacion.verificadoPago === 4){
                console.log('*********************************************si')
                const idPago = await PagoQueries.findByIdVerificacion(autorizacion.id);
                console.log(idPago)
                await PagoQueries.updateEdoPago({idEstadoPago: 1}, idPago.idPago);
            }
            if (+estado === 2 || +estado === 5) {
                autorizacion.numero++
            }
            if (autorizacion.idEstadoVerificacion === 1) {
                autorizacion.idEstadoVerificacion = 2
            }
            if (autorizacion.verificadoCliente === 4 && autorizacion.verificadoRegistro === 4 && autorizacion.verificadoProducto === 4 && autorizacion.verificadoPago === 4) {
                await RegistroEntity.update({idEstadoPoliza: 6}, {
                    where: {id: autorizacion.idRegistro}
                });
                autorizacion.idEstadoVerificacion = 3;
                autorizacion.fechaConclusion = new Date();
            }
            await VerificacionRegistro.update({...autorizacion}, {
                where: {id: idVerificacionRegistro}
            });
            res.status(200).send();
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    // static getTablaConEmpleado = async (req: Request, res: Response) => {
    //     try {
    //         const {estadoVerificacion, idEmpleado, idFlujoPoliza, fechaInicio, fechaFin} = req.params;
    //         if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empelado', res);
    //         const permisos = await PermisosQueries.findPermisos(+idEmpleado);
    //         console.log(permisos);
    //         let registro;
    //         switch (+permisos.idPermisoVisualizacion) {
    //             case 1:
    //                 console.log("1");
    //                 registro = await VentanuevaQueries.findAll();
    //                 break;
    //             case 2:
    //                 console.log("2");
    //                 const departamentos = await UsuariosQuery.getDepartamentoByIdUsuario(permisos.idUsuario);
    //                 const aux = [];
    //                 for (const departamento of departamentos) {
    //                     aux.push(departamento.idDepartamento)
    //                 }
    //                 registro = await VentanuevaQueries.findAll();
    //                 // solicitudes = await SolicitudesInternoQueries.getAllByDepartamentos(fechaInicio, fechaFin, aux)
    //                 break;
    //             case 3:
    //                 console.log("3");
    //                 registro = await VentanuevaQueries.findAll();
    //                 // solicitudes = await SolicitudesInternoQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
    //                 break;
    //         }
    //         // return res.json(registro.filter(registro => {
    //         //     return registro
    //         // }));
    //         return  res.json(registro);
    //     } catch (e) {
    //         return res.status(500).json({
    //             ok: false,
    //             msg: 'error revisar consola del back'
    //         });
    //     }
    // }

    static getTablaRenovaciones = async (req: Request, res: Response) => {
        try {
            const {estadoVerificacion, idEmpleado, idFlujoPoliza, fechaInicio, fechaFin} = req.params;
            if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empelado', res);
            const permisos = await PermisosQueries.findPermisos(+idEmpleado);
            // let registro;
            switch (+permisos.idPermisoVisualizacion) {
                case 1:
                    // registro = await VentanuevaQueries.findAll();
                    break;
                case 2:
                    console.log("2");
                    const departamentos = await UsuariosQuery.getDepartamentoByIdUsuario(permisos.idUsuario);
                    const aux = [];
                    for (const departamento of departamentos) {
                        // aux.push(departamento.idDepartamento)
                    }
                    // registro = await VentanuevaQueries.findAll();
                    // solicitudes = await SolicitudesInternoQueries.getAllByDepartamentos(fechaInicio, fechaFin, aux)
                    break;
                case 3:
                    console.log("3");
                    // registro = await VentanuevaQueries.findAll();
                    // solicitudes = await SolicitudesInternoQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                    break;
            }
            // return res.json(registro.filter(registro => {
            //     return registro
            // }));
            return  res.json();
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}

export default VentaNuevaController;
