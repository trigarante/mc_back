import { Request, Response } from "express";
import Cotizaciones from "../../../entity/verificaciones/venta-nueva/ventanuevaEntity";
import VentanuevaQueries from "../../../queries/verificaciones/venta-nueva/ventanuevaQueries";
import PermisosQueries from "../../../queries/permisos/permisosQueries";
import UsuariosQuery from "../../../queries/usuarios/usuariosQuery";

class RegistroController {
    static getByEmpleado = async (req: Request, res: Response) => {
        try {
            const idEmpleado = req.params.idEmpleado;
            // const registro = await VentanuevaQueries.findAll();
            return res.json();
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getByIdVarificacion = async (req: Request, res: Response) => {
        try {
            const idVerificacion = req.params.idVerificacion;
            const verificacion = await Cotizaciones.findOne({
                where: {
                    id: idVerificacion,
                }
            });
            return res.json(verificacion);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static putVerificacion = async (req: Request, res: Response) => {
        try {
            const id = +req.params.id;
            const updateVerificacion = req.body;
            const verificacion = await Cotizaciones.findByPk(id);
            if(verificacion) {
                await verificacion.update(updateVerificacion)
            }
            return res.json(verificacion);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static postVerificacion = async (req: Request, res: Response) => {
        try {
            return res.json(true);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static putDocumentoVerificado = async (req: Request, res: Response) => {
        try {
            return res.json(true);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getTabla = async (req: Request, res: Response) => {
        try {
            const {estadoVerificacion, idEmpleado, idFlujoPoliza} = req.params;
            // if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empelado', res);
            const permisos = await PermisosQueries.findPermisos(+idEmpleado);
            console.log(permisos);
            // let registro;
            switch (+permisos.idPermisoVisualizacion) {
                case 1:
                    // registro = await VentanuevaQueries.findAll();
                    break;
                case 2:
                    const departamentos = await UsuariosQuery.getDepartamentoByIdUsuario(permisos.idUsuario);
                    const aux = [];
                    for (const departamento of departamentos) {
                        // aux.push(departamento.idDepartamento)
                    }
                    // registro = await VentanuevaQueries.findAll();
                    // solicitudes = await SolicitudesInternoQueries.getAllByDepartamentos(fechaInicio, fechaFin, aux)
                    break;
                case 3:
                    // registro = await VentanuevaQueries.findAll();
                    // solicitudes = await SolicitudesInternoQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                    break;
            }
            // return res.json(registro.filter(registro => {
            //     return registro
            // }));
            return  res.json();
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getTablaConEmpleado = async (req: Request, res: Response) => {
        try {
            const {estadoVerificacion, idEmpleado, idFlujoPoliza} = req.params;
            // if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empelado', res);
            const permisos = await PermisosQueries.findPermisos(+idEmpleado);
            console.log(permisos);
            // let registro;
            switch (+permisos.idPermisoVisualizacion) {
                case 1:
                    console.log("1");
                    // registro = await VentanuevaQueries.findAll();
                    break;
                case 2:
                    console.log("2");
                    const departamentos = await UsuariosQuery.getDepartamentoByIdUsuario(permisos.idUsuario);
                    const aux = [];
                    for (const departamento of departamentos) {
                        // aux.push(departamento.idDepartamento)
                    }
                    // registro = await VentanuevaQueries.findAll();
                    // solicitudes = await SolicitudesInternoQueries.getAllByDepartamentos(fechaInicio, fechaFin, aux)
                    break;
                case 3:
                    console.log("3");
                    // registro = await VentanuevaQueries.findAll();
                    // solicitudes = await SolicitudesInternoQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                    break;
            }
            // return res.json(registro.filter(registro => {
            //     return registro
            // }));
            return  res.json();
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getTablaRenovaciones = async (req: Request, res: Response) => {
        try {
            return res.json(true);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}

export default RegistroController;
