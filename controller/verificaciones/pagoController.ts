import { Request, Response } from "express";
import PagoQueries from "../../queries/verificaciones/pagoQueries";
import EmpleadoQueries from "../../queries/empleado/empleadoQueries";
import pagos from "../../entity/pagos/pagoEntity";
import PermisosQueries from "../../queries/permisos/permisosQueries";
import VentanuevaQueries from "../../queries/verificaciones/venta-nueva/ventanuevaQueries";
import UsuariosQuery from "../../queries/usuarios/usuariosQuery";
import verificacionPago from "../../entity/verificaciones/verificacionPagoEntity";
import AplicacionesViewQueries from "../../queries/aplicacionesView/aplicacionesView";
const generales = require('../../general/function');

class PagoController {
    static getAllByIdEmpleado = async (req: Request, res: Response) => {
        try {
            const {tipo, idEmpleado, fechaInicio, fechaFin} = req.params;
            if (!(tipo && idEmpleado)) return generales.manejoErrores('No se envio el id empelado', res);
            const permisos = await PermisosQueries.findPermisos(+idEmpleado);
            let pago;
            let aux2;
            switch (+tipo) {
                case 1:
                    aux2 = `operaciones."verificacionPago"."idEstadoVerificacion" = 1 AND operaciones."verificacionPago"."verificadoPago" = 1`
                    break;
                case 2:
                    aux2 = `operaciones."verificacionPago"."idEstadoVerificacion" = 2 AND (operaciones."verificacionPago"."verificadoPago" = 2 OR operaciones."verificacionPago"."verificadoPago" = 3 OR operaciones."verificacionPago"."verificadoPago" = 5)`
                    break;
                case 3:
                    aux2 = `operaciones."verificacionPago"."idEstadoVerificacion" = 2 AND operaciones."verificacionPago"."verificadoPago" = 3`
                    break;
                case 4:
                    aux2 = `operaciones."verificacionPago"."idEstadoVerificacion" = 3 AND operaciones."verificacionPago"."verificadoPago" = 4`
                    break;
            }
            if (+tipo !==  1 && permisos.idPermisoVisualizacion === 3) {
                aux2 += ` AND operaciones."verificacionPago"."idEmpleado" = ${idEmpleado}`
            }
            switch (+permisos.idPermisoVisualizacion) {
                case 1:
                    pago = await PagoQueries.findAllPago(fechaInicio, fechaFin, aux2);

                    break;
                case 2:
                    // if (permisos.idPuesto === 7) {
                    //     // Supervisores solo deben de ver lo de sus chavos
                    //     const hijos = await PermisosQueries.getPlazasHijo(permisos.id);
                    //     const aux = [];
                    //     for (const hijo of hijos) {
                    //         // @ts-ignore
                    //         aux.push(hijo.id)
                    //     }
                    //     if (hijos.length === 0){
                    //         // @ts-ignore
                    //         aux.push(+idEmpleado)
                    //     }
                    //     pago = await PagoQueries.findAllByHijos(fechaInicio, fechaFin, aux, aux2)
                    // } else {
                        const departamentos = await UsuariosQuery.getDepartamentoByIdUsuario(permisos.idUsuario);
                        const aux = [];
                        for (const departamento of departamentos) {
                            // @ts-ignore
                            aux.push(departamento.idDepartamento)
                        }
                        if (departamentos.length === 0){
                            aux.push(permisos.idDepartamento)
                        }
                        pago = await PagoQueries.findAllByIdDepartamento(fechaInicio, fechaFin, aux, aux2);
                    // }
                    break;
                case 3:
                    pago = await PagoQueries.findByIdEmpledo(fechaInicio, fechaFin, permisos.idDepartamento, idEmpleado, aux2);
                    break;
            }
            if (pago === undefined || pago === null)
                pago = [];
            return res.json(pago);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getById = async (req: Request, res: Response) => {
        try {
            const id = req.params.id;
            const pago = await PagoQueries.findAllByid(id);
            return res.json(pago);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static updatePago = async (req: Request, res: Response) => {
        try {
            const json = {
                idEstadoVerificacion: req.body.idEstadoVerificacion,
                idEmpleado: req.body.idEmpleado,
                fechaInicio: req.body.fechaInicio,
            };
            await verificacionPago.update(json,{
                where: {id: req.body.id}
            });
            return res.json("Se actualizo correctamente");
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static activateVerificado = async (req: Request, res: Response) => {
        try {
            const idVerificacionPago = req.params.idVerificacionPago;
            console.log('idVerificacionPago');
            console.log(idVerificacionPago);
            const verificacion = await verificacionPago.findByPk(idVerificacionPago);
            console.log('verificacion');
            console.log(verificacion);
            const pago = await pagos.findByPk(verificacion?.getDataValue("idPago"));
            console.log('pago');
            console.log(pago);
            if(pago?.getDataValue("idEstadoPago") == 2) {
                pago.setDataValue("idEstadoPago", 1);
                pago.save();
            }
            verificacion?.setDataValue("verificadoPago", 4);
            verificacion?.setDataValue("idEstadoVerificacion", 3);
            verificacion?.setDataValue("fechaConclusion", new Date());
            console.log('verificacionX2');
            console.log(verificacion);
            await verificacion?.save();
            return res.json(verificacion);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}

export default PagoController;
