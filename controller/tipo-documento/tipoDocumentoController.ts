import {Request, Response} from 'express';
import TipoDocumentos from "../../entity/tipo-documentos/tipoDocumentosEntity";

class TipoDocumentoController {
    static getTipoDocumentoByIdTabla = async (req: Request, res: Response) => {
        try {
            const idTabla = req.params.idTabla;
            const tipoDocumentos = await TipoDocumentos.findAll({
                where: {idTabla, activo: 1}
            });
            return res.json(tipoDocumentos)
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}
export default TipoDocumentoController;
