import {Request, Response} from 'express';
import polizasTotalesQueries from "../../queries/polizas-totales/polizasTotalesQueries";

class PolizasTotalesController {
    static getPoliza = async (req: Request, res: Response) => {
        try {
           const id = req.params.id;
            const polizasT = await polizasTotalesQueries.findAllPolizas();
            return res.json(polizasT)
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}
export default PolizasTotalesController;
