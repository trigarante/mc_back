import {Request, Response} from 'express';
import CatalogoErroresQueries from "../../queries/catalogo-errores/catalogoErroresQueries";
import CatalogoErrores from "../../entity/catalogo-errores/catalogoErroresEntity";

class CatalogoErroresController {
    static getAllCatalogoErrores = async (req: Request, res: Response) => {
        try {
            const catalogoErroresQueries = await CatalogoErroresQueries.getCatalogoErrores();
            return res.json(catalogoErroresQueries);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getAllCatalogoErroresById = async (req: Request, res: Response) => {
        try {
            const idCatalogErrores = req.params.id;
            const catalogoErroresQueries = await CatalogoErroresQueries.getCatalogoErroresById(idCatalogErrores);
            return res.json(catalogoErroresQueries);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getCatalogoErroresCampoCorregir= async (req: Request, res: Response) => {
        try {
            const idCampoCorregir = req.params.idCampoCorregir;
            const catalogoErrores = await CatalogoErrores.findAll({
                where: {
                    idCampoCorregir,
                    activo: 1,
                }
            });
            return res.json(catalogoErrores);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static postCatalogoErrores= async (req: Request, res: Response) => {
        try {
            const catalogoErrores = await CatalogoErrores.create(req.body);
            return res.json(catalogoErrores);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static putCatalogoErrores= async (req: Request, res: Response) => {
        try {
            const catalogoErrores = await CatalogoErrores.update(req.body, {
                where: {id: req.body.id}
            });
            return res.json(catalogoErrores);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}
export default CatalogoErroresController;
