import { Request, Response } from "express";
import ErroresAnexosVPago from "../../entity/errores-anexsos/erroresAnexosVPago";
import verificacionPago from "../../entity/verificaciones/verificacionPagoEntity";
import pagos from "../../entity/pagos/pagoEntity";

class ErroresSubsecuentesAnexosController {
    static getPCorregir = async (req: Request, res: Response) => {
        try {
            const idVerificacionPago = req.params.idVerificacionPago;
            const erroresAnexsosVP = await ErroresAnexosVPago.findAll({
                where: {
                    idVerificacionPago,
                    idEstadoCorreccion: 1
                }
            });
            return res.json(erroresAnexsosVP);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static createErroresAVP = async (req: Request, res: Response) => {
        try {
            const verificacionPagoBody = req.body;
            const erroresAnexsosVP = await ErroresAnexosVPago.create(verificacionPagoBody);
            const verificacionPagoResult = await verificacionPago.findByPk(erroresAnexsosVP.getDataValue("idVerificacionPago"));
            const pago = await pagos.findOne({
                where:{id: verificacionPagoResult?.getDataValue("idPago")}
            });
            verificacionPagoResult?.setDataValue("numero", verificacionPagoResult?.getDataValue("numero") + 1);
            verificacionPagoResult?.setDataValue("verificadoPago", 2);
            await verificacionPagoResult?.save();
            await pago?.save();
            return res.json(erroresAnexsosVP);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static updateEstadoCorreccion = async (req: Request, res: Response) => {
        try {
            const idErrorAnexoPago = req.params.idErrorAnexoPago;
            const errorUpdate = await ErroresAnexosVPago.findOne({where:{idErrorAnexoPago}});
            errorUpdate?.setDataValue("idEstadoCorreccion", 2);
            errorUpdate?.setDataValue("fechaCorreccion", new Date());
            await errorUpdate?.save();
            const autorizacionPago = await verificacionPago.findOne({where:{idVerificacionPago: errorUpdate?.getDataValue("idVerificacionPago")}});
            autorizacionPago?.setDataValue("verificadoPago", 3);
            await autorizacionPago?.save();
            return res.json("Datos ingresados correctamente");
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}

export default ErroresSubsecuentesAnexosController;
