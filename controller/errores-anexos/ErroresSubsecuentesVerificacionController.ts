import { Request, Response } from "express";
import ErroresVerificacionPago from "../../entity/errores-anexsos/erroresVerificacionPagoEntity";
import VerificacionPago from "../../entity/verificaciones/verificacionPagoEntity";
import pagos from "../../entity/pagos/pagoEntity";

class ErroresSubsecuentesVController {
    static getPCorregir = async (req: Request, res: Response) => {
        try {
            const idVerificacionPago = req.params.idVerificacionPago;
            const erroresVerificacionPago = await ErroresVerificacionPago.findAll({
                where:{idVerificacionPago, idEstadoCorreccion: 1}
            });
            return res.json(erroresVerificacionPago);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static createErroresVP = async (req: Request, res: Response) => {
        try {
            const erroresVerificacionPago =  req.body;
            const erroresRes = await ErroresVerificacionPago.create(erroresVerificacionPago);
            const verificacionPago = await VerificacionPago.findOne({where: {idVerificacionPago: erroresVerificacionPago.idVerificacionPago}});
            const pago = await pagos.findOne({
                where:{id: verificacionPago?.getDataValue("idPago")}
            });
            pago?.setDataValue("idEstadoPago", 2);
            verificacionPago?.setDataValue("numero", pago?.getDataValue("numero") + 1);
            verificacionPago?.setDataValue("idEstadoPago", 5);
            await verificacionPago?.save();
            await pago?.save();
            return res.json(erroresRes);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static updateEstadoCorreccion = async (req: Request, res: Response) => {
        try {
            const idErrorAnexoPago = req.params.idErrorAnexoPago;
            const errorUpdate = await ErroresVerificacionPago.findOne({where:{idErrorAnexoPago}});
            errorUpdate?.setDataValue("idEstadoCorreccion", 2);
            errorUpdate?.setDataValue("fechaCorreccion", new Date());
            await errorUpdate?.save();
            const autorizacionPago = await VerificacionPago.findOne({where:{idVerificacionPago: errorUpdate?.getDataValue("idVerificacionPago")}});
            autorizacionPago?.setDataValue("verificadoPago", 3);
            await autorizacionPago?.save();
            return res.json(errorUpdate);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}

export default ErroresSubsecuentesVController;
