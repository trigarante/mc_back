import { Request, Response } from "express";
import ErroresVerificacion from "../../entity/errores-anexsos/erroresVerificacionEntity";
import PenalizacionesVerificacionEntity from "../../entity/verificaciones/penalizacionesVerificacionEntity";
import CatalogoErroresQueries from "../../queries/catalogo-errores/catalogoErroresQueries";
import VerificacionRegistro from "../../entity/verificaciones/verificacionRegistroEntity";
import CatalogoErroresDocsQueries from "../../queries/verificaciones/catalogoErroresDocsQueries";
const generales = require( '../../general/function');

class ErroresVerificacionController {
    static getAll = async (req: Request, res: Response) => {
        try {
            const erroresVerificacion = await ErroresVerificacion.findAll();
            return res.json(erroresVerificacion);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getErrores = async (req: Request, res: Response) => {
        try {
            const {idVerificacionRegistro, idTipoDocumento} = req.params;
            const erroresVerificacion = await ErroresVerificacion.findAll({
                where:{idVerificacionRegistro, idTipoDocumento, idEstadoCorreccion: 1}
            });
            return res.json(erroresVerificacion);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getHistoricoErrores = async (req: Request, res: Response) => {
        try {
            const {idVerificacionRegistro, idTipoDocumento} = req.params;
            const erroresVerificacion = await ErroresVerificacion.findAll({
                where:{idVerificacionRegistro, idTipoDocumento}
            });
            return res.json(erroresVerificacion);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }


    static createErroresVerifiacion = async (req: Request, res: Response) => {
        try {
            const jsonPenalizaciones = req.body;
            await ErroresVerificacion.create(req.body);
            let count = 1;
            for(const correccion of req.body.correcciones) {
               if (correccion.idCatalogoErrores) {
                   const penalizable = correccion.penalizable;
                   const penalizacionesPCampo = await PenalizacionesVerificacionEntity.findAll({
                       where:{idVerificacionRegistro: jsonPenalizaciones.idVerificacionRegistro, idCatalogoErrores: correccion.idCatalogoErrores}, raw: true
                   })
                   if(penalizacionesPCampo.length === 0 && penalizable) {
                       const penalizacion = await CatalogoErroresQueries.getCatalogoErroresById(correccion.idCatalogoErrores);
                       const arreglo = {
                           idVerificacionRegistro: jsonPenalizaciones.idVerificacionRegistro,
                           idCatalogoErrores: penalizacion.id,
                           montoPenalizacionEjecutivo: penalizacion.penalizacionEjecutivo,
                           montoPenalizacionSupervisor: penalizacion.penalizacionSupervisor
                       };
                       await PenalizacionesVerificacionEntity.create(arreglo)
                   }
               }
                count++;
            }
            const verificacionRegistro = await VerificacionRegistro.findByPk(jsonPenalizaciones.idVerificacionRegistro, {raw: true});
            let json = {};
            switch (jsonPenalizaciones.idTipoDocumento) {
                case 1:
                    json['verificadoCliente'] = 5;
                    break;
                case 2:
                    json['verificadoRegistro'] = 5;
                    break;
                case 3:
                    json['verificadoProducto'] = 5;
                    break;
                case 4:
                    json['verificadoPago'] = 5;
                    break;
            }
            json['numero']  = verificacionRegistro.numero + 1;
            if(verificacionRegistro.idEstadoVerificacion === 1) {
                json['idEstadoVerificacion'] = 2;
            }
            await VerificacionRegistro.update(json, {where: {id: jsonPenalizaciones.idVerificacionRegistro}});
            return res.json();
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back',
            });
        }
    }

    static cancelarCorreccion = async (req: Request, res: Response) => {
        try {
            const {idVerificacionRegistro, idTipoDocumento} = req.params;
            if (!(idVerificacionRegistro && idTipoDocumento)) return generales.manejoErrores('No se enviaron los id', res);
            const errorUpdate = await ErroresVerificacion.findOne({
                where: {idVerificacionRegistro, idTipoDocumento, idEstadoCorreccion: 1}
            });
            const verificacionRegistro = await VerificacionRegistro.findByPk(idVerificacionRegistro);
            errorUpdate?.setDataValue("idEstadoCorreccion", 3);
            errorUpdate?.setDataValue("fechaCorreccion", new Date());
            switch (errorUpdate?.getDataValue("idTipoDocumento")) {
                case 1: verificacionRegistro?.setDataValue("verificadoCliente", 3);
                    break;
                case 2: verificacionRegistro?.setDataValue("verificadoRegistro", 3);
                    break;
                case 3: verificacionRegistro?.setDataValue("verificadoProducto", 3);
                    break;
                case 4: verificacionRegistro?.setDataValue("verificadoPago", 3);
                    break;
            }
            await errorUpdate?.save();
            await verificacionRegistro?.save();
            return res.json(errorUpdate);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static updateEstadoCorreccion = async (req: Request, res: Response) => {
        try {
            const idErrorVerificacion = req.params.idErrorVerificacion;
            const errorUpdate = await ErroresVerificacion.findByPk(idErrorVerificacion);
            const objetoJSON = errorUpdate?.getDataValue("correcciones");
            const fechaCreacionErrorMillis = new Date(errorUpdate?.getDataValue("fechaCreacion")).getTime();
            const fechaHoyMillis = new Date().getTime();
            for(let errorAV of objetoJSON) {
                if(errorAV.idCatalogoErrores) {
                    const idCatalogo = errorAV.idCatalogoErrores;
                    const catalogo = await CatalogoErroresDocsQueries.findAllCatalogo(idCatalogo);
                    const penalizaciones = await PenalizacionesVerificacionEntity.findAll({
                        where: {idVerificacionRegistro: errorUpdate?.getDataValue("idVerificacionRegistro"), idCatalogo, idEstadoPenalizacion: 1}
                    });
                    if(penalizaciones) {
                        if(fechaCreacionErrorMillis + (1000 * 60 * 60 * catalogo.horasResolucion) > fechaHoyMillis){
                            for(let penalizacion of penalizaciones){
                                penalizacion.setDataValue("montoAplicado", +penalizacion.getDataValue("montoPenalizacionEjecutivo") * 0.40);
                                penalizacion.setDataValue("montoAplicadoSupervisor", +penalizacion.getDataValue("montoPenalizacionSupervisor") * 0.40);
                            }
                        } else {
                            for(let penalizacion of penalizaciones){
                                penalizacion.setDataValue("montoAplicado", +penalizacion.getDataValue("montoPenalizacionEjecutivo"));
                                penalizacion.setDataValue("montoAplicadoSupervisor", +penalizacion.getDataValue("montoPenalizacionSupervisor"));
                            }
                        }
                        for(let penalizacion of penalizaciones){
                            penalizacion.setDataValue("idEstadoPenalizacion", 2);
                            penalizacion.setDataValue("fechaAplicacion", new Date().getTime());
                            penalizacion.save();
                        }
                    }
                }
            }
            errorUpdate?.setDataValue("idEstadoCorreccion", 2);
            errorUpdate?.setDataValue("fechaCorreccion", new Date().getTime());
            return res.json(errorUpdate);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}

export default ErroresVerificacionController;
