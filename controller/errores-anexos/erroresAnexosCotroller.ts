import {Request, Response} from "express";
import ErroresAnexosV from "../../entity/errores-anexsos/erroresAnexosVEntity";
import PenalizacionesAV from "../../entity/penalizaciones/penalizacionesAnezosVEntity";
import VerificacionRegistro from "../../entity/verificaciones/verificacionRegistroEntity";
import PagoQueries from "../../queries/verificaciones/pagoQueries";
import pagos from "../../entity/pagos/pagoEntity";
import CatalogoErroresDocsQueries from "../../queries/verificaciones/catalogoErroresDocsQueries";
const generales = require('../../general/function');

class ErroresAnexosController {
    static getAll = async (req: Request, res: Response) => {
        try {
            const erroresAnexsos = await ErroresAnexosV.findAll();
            return res.json(erroresAnexsos);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getByIdVerificaiconRegistro = async (req: Request, res: Response) => {
        try {
            const {idVerificacionRegistro, idTipoDocumento} = req.params;
            const erroresAnexsos = await ErroresAnexosV.findAll({
                where: {idVerificacionRegistro, idTipoDocumento, idEstadoCorreccion: 1}
            });
            return res.json(erroresAnexsos);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getVerificacion = async (errores) => {
        try {
            return await PenalizacionesAV.findAll({
                where: {
                    idVerificacionRegistro: errores.idVerificacionRegistro,
                    idCatalogoErroresDocumentos: errores.idCatalogoErroresDocumentos,
                },
                raw: true
            });
        } catch (e) {
            return {
                ok: false,
                msg: 'error revisar consola del back',
                error: e
            };
        }
    }

    static getErrores = async (idVerificacionRegistro, idCatalogoErrores) => {
        try {
            const penalizacion = await PenalizacionesAV.findAll({
                where: {
                    idVerificacionRegistro,
                    idCatalogoErrores
                }
            });
            return penalizacion;
        } catch (e) {
            return {
                ok: false,
                msg: 'error revisar consola del back'
            };
        }
    }
    static cancelarCorreccion = async (req: Request, res: Response) => {
        try {
            const {idVerificacionRegistro, idTipoDocumento} = req.params;
            if (!(idVerificacionRegistro && idTipoDocumento)) return generales.manejoErrores('No se enviaron los id', res);
            const errorUpdate = await ErroresAnexosV.findOne({
                where: {idVerificacionRegistro, idTipoDocumento, idEstadoCorreccion: 1}
            });
            const verificacionRegistro = await VerificacionRegistro.findByPk(idVerificacionRegistro);
            errorUpdate?.setDataValue("idEstadoCorreccion", 3);
            errorUpdate?.setDataValue("fechaCorreccion", new Date());

            switch (errorUpdate?.getDataValue("idTipoDocumento")) {
                case 1: verificacionRegistro?.setDataValue("verificadoCliente", 3);
                    break;
                case 2: verificacionRegistro?.setDataValue("verificadoRegistro", 3);
                    break;
                case 3: verificacionRegistro?.setDataValue("verificadoProducto", 3);
                    break;
                case 4:
                    const verificacionRegistroView = await PagoQueries.findByIdVerificacion(errorUpdate?.getDataValue("idVerificacionRegistro"));
                    verificacionRegistro?.setDataValue("verificadoPago", 3);
                    const pago = await pagos.findByPk(verificacionRegistroView.idPago);
                    pago?.setDataValue("idEstadoPago", 1)
                    pago?.save();
                    break;
            }
            await errorUpdate?.save();
            await verificacionRegistro?.save();
            return res.json(errorUpdate);
        } catch (e) {
            return {
                ok: false,
                msg: 'error revisar consola del back'
            };
        }
    }

    static updateEstadoCorreccion = async (req: Request, res: Response) => {
        try {
            const idErroresAnexosV = req.params.idErroresAnexosV;
            const errorUpdate = await ErroresAnexosV.findByPk(idErroresAnexosV);
            const objetoJSON = errorUpdate?.getDataValue("correcciones");
            const fechaCreacionMillis = new Date().getTime();
            const fechaCreacionCalendar = new Date(errorUpdate?.getDataValue("fechaCreacion"));
            for(let errorAV of objetoJSON) {
                if(errorAV.idComentarioCorreccionDocumento) {
                    const idCatalogo = errorAV.idComentarioCorreccionDocumento;
                    const catalogo = await CatalogoErroresDocsQueries.findAllCatalogo(idCatalogo);
                    const penalizaciones = await PenalizacionesAV.findAll({
                        where: {idVerificacionRegistro: errorUpdate?.getDataValue("idVerificacionRegistro"), idCatalogoErroresDocumentos: idCatalogo, idEstadoPenalizacion: 1}
                    });
                    if(penalizaciones) {
                        if(fechaCreacionCalendar.getTime() + (1000 * 60 * 60 * catalogo.horasResolucion) > fechaCreacionMillis){
                            for(let penalizacion of penalizaciones){
                                penalizacion.setDataValue("montoAplicado", +penalizacion.getDataValue("montoPenalizacionEjecutivo") * 0.40);
                                penalizacion.setDataValue("montoAplicadoSupervisor", +penalizacion.getDataValue("montoPenalizacionSupervisor") * 0.40);
                            }
                        } else {
                            for(let penalizacion of penalizaciones){
                                penalizacion.setDataValue("montoAplicado", +penalizacion.getDataValue("montoPenalizacionEjecutivo"));
                                penalizacion.setDataValue("montoAplicadoSupervisor", +penalizacion.getDataValue("montoPenalizacionSupervisor"));
                            }
                        }
                        for(let penalizacion of penalizaciones){
                            penalizacion.setDataValue("idEstadoPenalizacion", 2);
                            penalizacion.setDataValue("fechaAplicacion", new Date());
                            penalizacion.save();
                        }
                    }
                }
            }
            errorUpdate?.setDataValue("idEstadoCorreccion", 2);
            errorUpdate?.setDataValue("fechaCorreccion", new Date());
            await errorUpdate?.save();
            return res.json(errorUpdate);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}

export default ErroresAnexosController;
