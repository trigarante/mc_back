import {Request, Response} from 'express';
import VerificacionRegistro from "../../entity/verificaciones/verificacionRegistroEntity";
import RegistroEntity from "../../entity/registro/registroEntity";
import LiberacionCorreccionesConcluidas from "../../entity/liberacion-corregidas/liberacionCorregidasEntity";

class LiberacinoCorregidasController {
    static postLiberacionCorregidas = async (req: Request, res: Response) => {
        try {
            const liberacionCorreccionesConcluidas = req.body;
            // const numeros = req.header("authorization");
            const verificacionRegistro = await VerificacionRegistro.findByPk(liberacionCorreccionesConcluidas.idVerificacionRegistro);
            const registro = await RegistroEntity.findByPk(verificacionRegistro?.getDataValue("idRegistro"));
            const opciones = JSON.parse(req.body.datos)
            console.log('opciones',opciones.length)
            console.log('opcionesArreglo',opciones[0])
            for (let i = 0; i < opciones.length; i++){
                console.log('opciones for', opciones[i])
                switch (opciones[i]) {
                    case 'Cliente': verificacionRegistro?.setDataValue("verificadoCliente", 3);
                        break;
                    case 'Producto': verificacionRegistro?.setDataValue("verificadoProducto", 3);
                        break;
                    case 'Registro': verificacionRegistro?.setDataValue("verificadoRegistro", 3);
                        break;
                    case 'Pago': verificacionRegistro?.setDataValue("verificadoPago", 3);
                        break;
                }
            }
            // if(numeros == null) {
            //     const arrayNumeros = numeros.split(",");
            //     for (const numero of arrayNumeros) {
            //         switch (numero) {
            //             case "0": verificacionRegistro?.setDataValue("verificadoCliente", 3);
            //                 break;
            //             case "1": verificacionRegistro?.setDataValue("verificadoProducto", 3);
            //                 break;
            //             case "2": verificacionRegistro?.setDataValue("verificadoRegistro", 3);
            //                 break;
            //             default: verificacionRegistro?.setDataValue("verificadoPago", 3);
            //                 break;
            //         }
            //     }
            // }
            verificacionRegistro?.setDataValue("idEstadoVerificacion", 2);
            verificacionRegistro?.setDataValue("fechaConclusion", null);
            registro?.setDataValue("idEstadoPoliza", 5)
            registro?.save();
            verificacionRegistro?.save();
            const liberacion = await LiberacionCorreccionesConcluidas.create(liberacionCorreccionesConcluidas).catch(e => console.log(e));;
            return res.json(liberacion);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}
export default LiberacinoCorregidasController;
