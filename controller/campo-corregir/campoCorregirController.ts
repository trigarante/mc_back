import {Request, Response} from 'express';
import CampoCorregir from "../../entity/campo-corregir/campoCorregirEntity";
import {Op} from "sequelize";
const generales = require('../../general/function');

class CampoCorregirController {
    static getCampoCorregirByIdTabla = async (req: Request, res: Response) => {
        try {
            const idTabla = req.params.idTabla;
            const tipoDocumentos = await CampoCorregir.findAll({
                where: {idTabla, activo: 1}
            });
            return res.json(tipoDocumentos)
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getCampoCorregirByIdTablaMoral = async (req: Request, res: Response) => {
        try {
            const {idTabla} = req.params;
            if (!idTabla) return generales.manejoErrores('No se envio el id tabla', res);
            const campoCorregir = await CampoCorregir.findAll({
                where: {
                    activo: 1,
                    idTabla,
                    id: {
                        [Op.notIn]: [5, 6, 13, 16, 29]
                    }
                },
            });
            res.status(200).send(campoCorregir);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}
export default CampoCorregirController;
