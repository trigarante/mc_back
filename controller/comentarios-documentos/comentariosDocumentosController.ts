import {Request, Response} from 'express';
import ComentariosDocumentosQueries from "../../queries/comentarios-documentos/comentariosDocumentosQueries";
import ComentariosDocumentos from "../../entity/comentarios-documentos/comentariosDocumentosEntity";
const generales = require('../../general/function');

class ComentariosDocumentosController {
    static getAllComentariosDocumentos = async (req: Request, res: Response) => {
        try {
            const comentariosDocumentosQueries = await ComentariosDocumentosQueries.getComentariosDocumentos();
            return res.json(comentariosDocumentosQueries);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getAllComentariosDocumentosById = async (req: Request, res: Response) => {
        try {
            const idCatalogErrores = req.params.id;
            const comentariosDocumentosQueries = await ComentariosDocumentosQueries.getComentariosDocumentosById(idCatalogErrores);
            return res.json(comentariosDocumentosQueries);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static getCatalogoErroresCampoCorregir= async (req: Request, res: Response) => {
        try {
            const {idTipoDocumento} = req.params;
            if (!idTipoDocumento) return generales.manejoErrores('no se envio el id', res)
            const catalogoErrores = await ComentariosDocumentos.findAll({
                where: {
                    idTipoDocumento,
                    activo: 1,
                }
            });
            return res.json(catalogoErrores);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static postCatalogoErrores= async (req: Request, res: Response) => {
        try {
            const catalogoErrores = await ComentariosDocumentos.create(req.body);
            return res.json(catalogoErrores);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }

    static putCatalogoErrores= async (req: Request, res: Response) => {
        try {
            const catalogoErrores = await ComentariosDocumentos.update(req.body, {
                where: {id: req.body.id}
            });
            return res.json(catalogoErrores);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}
export default ComentariosDocumentosController;
