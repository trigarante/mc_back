import {Request, Response} from 'express';
import VerificacionRegistro from "../../entity/verificaciones/verificacionRegistroEntity";
import LiberacionErroresConcluidas from "../../entity/liberacion-errores/liberacionErroresEntity";

class LiberacinoErroresController {
    static postLiberacionErrores = async (req: Request, res: Response) => {
        try {
            const liberacionErrores = req.body;
            const verificacion = await VerificacionRegistro.findByPk(liberacionErrores.idVerificacionRegistro);
            const liberacionErroresConcluidas = LiberacionErroresConcluidas.create(liberacionErrores);
            switch (liberacionErrores.idTipoDocumento) {
                case 1: verificacion?.setDataValue("verificadoCliente", 3);
                    break;
                case 2: verificacion?.setDataValue("verificadoRegistro", 3);
                    break;
                case 3: verificacion?.setDataValue("verificadoProducto", 3);
                    break;
                case 4: verificacion?.setDataValue("verificadoPago", 3);
                    break;
            }
            verificacion?.save()
            return res.json(liberacionErroresConcluidas);
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
    }
}
export default LiberacinoErroresController;
