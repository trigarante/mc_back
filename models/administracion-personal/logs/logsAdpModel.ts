import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const LogsAdpModel  = dbPostgres.define('logsAdpModel',
    {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        idEmpleadoModificante: {
            type: DataTypes.BIGINT,
            allowNull: false,
            validate: {
                isInt: true
            }
        },
        idEmpleado: {
            type: DataTypes.BIGINT,
            validate: {
                isInt: true
            }
        },
        idPlaza: {
            type: DataTypes.BIGINT,
            validate: {
                isInt: true
            }
        },
        idTipoModificacionAdp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true
            }
        },
        json: {
            type: DataTypes.JSON,
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        }
    },
    {
        tableName: "logsAdp",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default LogsAdpModel;
