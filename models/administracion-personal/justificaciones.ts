
import {DataTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

const Justificaciones = dbPostgres.define('justificacionesModel',
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        idEmpleado: {
            type: DataTypes.INTEGER,
        },
        idEmpleadoSolicitante: {
            type: DataTypes.INTEGER,
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        },
        fechaFin: {
            type: DataTypes.DATE,
        },
        fechaInicio: {
            type: DataTypes.DATE,
        },
        documento: {
            type: DataTypes.STRING,
        },
        razonSocial: {
            type: DataTypes.STRING,
        },
        idEstadoJustificacion: {
            type: DataTypes.INTEGER,
        },
        idMotivoJustificacion: {
            type: DataTypes.INTEGER,
        },
        idEmpleadoAutorizo: {
            type: DataTypes.INTEGER,
        },
        comentarios: {
            type: DataTypes.STRING,
        },
        comentariosAdp: {
            type: DataTypes.STRING,
        },
        idTipoJustificacion: {
            type: DataTypes.INTEGER,
        },
        folio: {
            type: DataTypes.STRING,
        },
        folioUnico: {
            type: DataTypes.SMALLINT,
        },
    },
    {
        tableName: "justificaciones",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default Justificaciones;
