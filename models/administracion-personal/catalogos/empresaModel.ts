import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const EmpresaModel = dbPostgres.define('empresaModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        alias: {
            type: DataTypes.STRING
        },
        registroPatronal: {
            type: DataTypes.STRING
        },
        direccion: {
            type: DataTypes.TEXT
        },
        activo: {
            type: DataTypes.INTEGER
        }
    },
    {
        tableName: "empresas",
        schema: "recursosHumanos",
        timestamps: false,
    }
);

export default EmpresaModel;
