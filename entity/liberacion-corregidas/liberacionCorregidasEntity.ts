import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class LiberacionCorreccionesConcluidas extends Model{}

LiberacionCorreccionesConcluidas.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idVerificacionRegistro: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    datos: {
        type: DataTypes.STRING,
    },
    fechaConclusionAnterior: {
        type: DataTypes.DATE,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
}, {
    timestamps: false,
    schema: 'generales',
    tableName: 'liberacionCorreccionesConcluidas',
    sequelize
});

export default LiberacionCorreccionesConcluidas;
