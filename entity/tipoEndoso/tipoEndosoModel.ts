import {DataTypes} from "sequelize";
import db from "../../db/connection";

const TipoEndosoModel = db.define('tipoEndoso', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    descripcion: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "tipoEndoso",
    schema: "generales",
    timestamps: false
})

export default TipoEndosoModel
