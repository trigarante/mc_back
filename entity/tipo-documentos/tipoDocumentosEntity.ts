import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class TipoDocumentos extends Model{}

TipoDocumentos.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idTabla: {
        type: DataTypes.BIGINT,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.INTEGER,
    },
}, {
    timestamps: false,
    schema: 'operaciones',
    tableName: 'tipoDocumentos',
    sequelize
});

export default TipoDocumentos;
