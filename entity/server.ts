import express, { Application } from 'express';
import cors from 'cors';
import db from '../db/connection';
import mainRoutes from '../routes/indexRoutes';
const morgan = require('morgan');
import helmet from 'helmet';
const fileUpload = require('express-fileupload');

class Server {

    private app: Application;
    private port: string;

    constructor() {
        this.app  = express();
        this.port = process.env.PORT || '8080';
        this.dbConnection();
        this.middlewares();
        this.routes();
    }
    async dbConnection() {
        try {
            await db.authenticate();
            console.log('Database online');
        } catch (error) {
            throw new Error('Error en la base de datos: ' + error);
        }
    }

    middlewares() {
        // Morgan
        this.app.use(morgan('tiny'));

        // CORS
        this.app.use( cors() );

        // Helmet
        this.app.use(helmet());

        // Disable header
        this.app.disable('x-powered-by');

        this.app.use(fileUpload());

        // Lectura del body
        this.app.use( express.json() );

        // Carpeta pública
        this.app.use( express.static('public') );
    }

    routes() {
        this.app.use('/mc', mainRoutes);
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port );
        });
    }
}

export default Server;
