import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class CampoCorregir extends Model{}

CampoCorregir.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idTabla: {
        type: DataTypes.BIGINT,
    },
    campo: {
        type: DataTypes.STRING,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.INTEGER,
    },
}, {
    timestamps: false,
    schema: 'operaciones',
    tableName: 'campoCorregir',
    sequelize
});

export default CampoCorregir;
