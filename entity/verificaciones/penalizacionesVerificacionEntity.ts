import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class PenalizacionesVerificacionEntity extends Model{}

PenalizacionesVerificacionEntity.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idVerificacionRegistro: {
        type: DataTypes.BIGINT,
    },
    idEstadoPenalizacion: {
        type: DataTypes.INTEGER,
    },
    idCatalogoErrores: {
        type: DataTypes.INTEGER,
    },
    montoPenalizacionEjecutivo: {
        type: DataTypes.INTEGER,
    },
    montoPenalizacionSupervisor: {
        type: DataTypes.INTEGER,
    },
    montoAplicado: {
        type: DataTypes.INTEGER,
    },
    montoAplicadoSupervisor: {
        type: DataTypes.INTEGER,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    fechaAplicacion: {
        type: DataTypes.DATE,
    },
}, {
    timestamps: false,
    schema: 'generales',
    tableName: 'penalizacionesVerificacion',
    sequelize
});

export default PenalizacionesVerificacionEntity;
