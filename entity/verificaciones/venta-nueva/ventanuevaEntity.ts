import {DataTypes, Model} from "sequelize";
import db from "../../../db/connection";

const sequelize = db;
class Cotizaciones extends Model{}

Cotizaciones.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idProducto: {
        type: DataTypes.BIGINT,
    },
    idPagina: {
        type: DataTypes.INTEGER,
    },
    idMedioDifusion: {
        type: DataTypes.INTEGER,
    },
    idTipoContacto: {
        type: DataTypes.INTEGER,
    },
    idEstadoCotizacion: {
        type: DataTypes.INTEGER,
    },
    fechaCreacion: {
        type: DataTypes.DATE,
    },
}, {
    timestamps: false,
    schema: 'operaciones',
    tableName: 'cotizaciones',
    sequelize
});

export default Cotizaciones;
