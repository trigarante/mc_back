import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class verificacionPago extends Model{}

verificacionPago.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idPago: {
        type: DataTypes.BIGINT,
    },
    idEstadoVerificacion: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.INTEGER,
    },
    verificadoPago: {
        type: DataTypes.INTEGER,
    },
    fechaCreacion: {
        type: DataTypes.DATE,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaConclusion: {
        type: DataTypes.DATE,
    },
}, {
    timestamps: false,
    schema: 'operaciones',
    tableName: 'verificacionPago',
    sequelize
});

export default verificacionPago;
