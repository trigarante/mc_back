import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class VerificacionRegistro extends Model{}

VerificacionRegistro.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idRegistro: {
        type: DataTypes.BIGINT,
    },
    idEstadoVerificacion: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.INTEGER,
    },
    verificadoCliente: {
        type: DataTypes.INTEGER,
    },
    verificadoProducto: {
        type: DataTypes.INTEGER,
    },
    verificadoRegistro: {
        type: DataTypes.INTEGER,
    },
    verificadoPago: {
        type: DataTypes.INTEGER,
    },
    verificadoInspeccion: {
        type: DataTypes.INTEGER,
    },
    fechaCreacion: {
        type: DataTypes.DATE,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaConclusion: {
        type: DataTypes.DATE,
    },
}, {
    timestamps: false,
    schema: 'operaciones',
    tableName: 'verificacionRegistro',
    sequelize
});

export default VerificacionRegistro;
