import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class ComentariosDocumentos extends Model{}

ComentariosDocumentos.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idTipoDocumento: {
        type: DataTypes.BIGINT,
    },
    idTiempoResolucion: {
        type: DataTypes.INTEGER,
    },
    penalizacionEjecutivo: {
        type: DataTypes.FLOAT,
    },
    penalizacionSupervisor: {
        type: DataTypes.FLOAT,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.INTEGER,
    },
}, {
    timestamps: false,
    schema: 'generales',
    tableName: 'catalogoErroresDocumentos',
    sequelize
});

export default ComentariosDocumentos;
