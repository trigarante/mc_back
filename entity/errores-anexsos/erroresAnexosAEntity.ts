import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class ErroresAnexsosA extends Model{}

ErroresAnexsosA.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idAutorizacionRegistro: {
        type: DataTypes.BIGINT,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idEstadoCorreccion: {
        type: DataTypes.INTEGER,
    },
    idEmpleadoCorreccion: {
        type: DataTypes.INTEGER,
    },
    idTipoDocumento: {
        type: DataTypes.INTEGER,
    },
    fechaCreacion: {
        type: DataTypes.DATE,
    },
    fechaCorreccion: {
        type: DataTypes.DATE,
    },
    correcciones: {
        type: DataTypes.JSON,
    },
}, {
    timestamps: false,
    schema: 'operaciones',
    tableName: 'erroresAnexosA',
    sequelize
});

export default ErroresAnexsosA;
