import {DataTypes, Model, QueryTypes} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class ErroresAnexosVPago extends Model{}

ErroresAnexosVPago.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idVerificacionPago: {
        type: DataTypes.BIGINT,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idEstadoCorreccion: {
        type: DataTypes.INTEGER,
    },
    idEmpleadoCorreccion: {
        type: DataTypes.INTEGER,
    },
    fechaCreacion: {
        type: DataTypes.DATE,
    },
    fechaCorreccion: {
        type: DataTypes.DATE,
    },
    correcciones: {
        type: DataTypes.JSON,
    },
}, {
    timestamps: false,
    schema: 'generales',
    tableName: 'erroresAnexosVPago',
    sequelize,
});

export default ErroresAnexosVPago;
