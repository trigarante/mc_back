import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class ErroresAnexsosAPago extends Model{}

ErroresAnexsosAPago.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idAutorizacionPago: {
        type: DataTypes.BIGINT,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idEstadoCorreccion: {
        type: DataTypes.INTEGER,
    },
    idEmpleadoCorreccion: {
        type: DataTypes.INTEGER,
    },
    idTipoDocumento: {
        type: DataTypes.INTEGER,
    },
    fechaCreacion: {
        type: DataTypes.DATE,
    },
    fechaCorreccion: {
        type: DataTypes.DATE,
    },
    correcciones: {
        type: DataTypes.JSON,
    },
}, {
    timestamps: false,
    schema: 'generales',
    tableName: 'erroresAnexosAPago',
    sequelize
});

export default ErroresAnexsosAPago;
