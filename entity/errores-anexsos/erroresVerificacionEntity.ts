import {DataTypes, Model, QueryTypes} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class ErroresVerificacion extends Model{}

ErroresVerificacion.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idVerificacionRegistro: {
        type: DataTypes.BIGINT,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idEstadoCorreccion: {
        type: DataTypes.INTEGER,
    },
    idTipoDocumento: {
        type: DataTypes.INTEGER,
    },
    idEmpleadoCorreccion: {
        type: DataTypes.INTEGER,
    },
    fechaCreacion: {
        type: DataTypes.DATE,
    },
    fechaCorreccion: {
        type: DataTypes.DATE,
    },
    correcciones: {
        type: DataTypes.JSON,
    },
}, {
    timestamps: false,
    schema: 'operaciones',
    tableName: 'erroresVerificacion',
    sequelize,
});

export default ErroresVerificacion;
