import {dbPostgres} from "../../configs/connection";
import {DataTypes} from "sequelize";

const LogsDesviadasModel  = dbPostgres.define('logsAdpModel',
    {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        idEmpleadoMC: {
            type: DataTypes.INTEGER,
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        },
        idRegistro: {
            type: DataTypes.BIGINT,
            validate: {
                isInt: true
            }
        },
        comentariosRechazo: {
            type: DataTypes.STRING,
        },
        idEstadoPago: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true
            }
        },
    },
    {
        tableName: "logsDesviadasMC",
        timestamps: false,
        schema: "operaciones"
    }
);

export default LogsDesviadasModel;
