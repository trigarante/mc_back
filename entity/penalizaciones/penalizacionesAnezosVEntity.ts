import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";
import {dbPostgres} from "../../configs/connection";


const PenalizacionesAV = dbPostgres.define('penalizacionesAV', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idVerificacionRegistro: {
        type: DataTypes.BIGINT,
    },
    idEstadoPenalizacion: {
        type: DataTypes.INTEGER,
    },
    idCatalogoErroresDocumentos: {
        type: DataTypes.INTEGER,
    },
    montoPenalizacionEjecutivo: {
        type: DataTypes.INTEGER,
    },
    montoPenalizacionSupervisor: {
        type: DataTypes.INTEGER,
    },
    montoAplicado: {
        type: DataTypes.INTEGER,
    },
    montoAplicadoSupervisor: {
        type: DataTypes.INTEGER,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    fechaAplicacion: {
        type: DataTypes.DATE,
    },
}, {
    timestamps: false,
    schema: 'generales',
    tableName: 'penalizacionesAnexosVerificacion',
});

export default PenalizacionesAV;
