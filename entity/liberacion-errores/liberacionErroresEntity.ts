import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class LiberacionErroresConcluidas extends Model{}

LiberacionErroresConcluidas.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idVerificacionRegistro: {
        type: DataTypes.INTEGER,
    },
    idTipoDocumento: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
}, {
    timestamps: false,
    schema: 'generales',
    tableName: 'liberacionErroresConcluidos',
    sequelize
});

export default LiberacionErroresConcluidas;
