import db from "../../db/connection";
import {DataTypes} from "sequelize";


const ClienteModel = db.define('cliente', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idPais: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    nombre: {
        type: DataTypes.STRING
    },
    paterno: {
        type: DataTypes.STRING
    },
    materno: {
        type: DataTypes.STRING
    },
    curp: {
        type: DataTypes.STRING
    },
    razonSocial: {
        type: DataTypes.INTEGER
    },
    cp: {
        type: DataTypes.STRING
    },
    idColonia: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    idColoniaPeru: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    calle: {
        type: DataTypes.STRING
    },
    correo: {
        type: DataTypes.STRING
    },
    numInt: {
        type: DataTypes.STRING
    },
    numExt: {
        type: DataTypes.STRING
    },
    genero: {
        type: DataTypes.STRING
    },
    telefonoFijo: {
        type: DataTypes.STRING
    },
    telefonoMovil: {
        type: DataTypes.STRING
    },
    fechaNacimiento: {
        type: DataTypes.DATE
    },
    contrasenaApp: {
        type: DataTypes.STRING
    },
    token: {
        type: DataTypes.STRING
    },
    corregido: {
        type: DataTypes.STRING
    },
    acceso: {
        type: DataTypes.STRING
    },
    archivo: {
        type: DataTypes.STRING
    },
    archivoSubido: {
        type: DataTypes.STRING
    },
    ruc: {
        type: DataTypes.STRING
    },
    rfc: {
        type: DataTypes.STRING
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
}, {
    tableName: "cliente",
    schema: "operaciones",
    timestamps: false
})

export default ClienteModel
