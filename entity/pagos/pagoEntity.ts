import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class pagos extends Model{}

pagos.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idRecibo: {
        type: DataTypes.BIGINT,
    },
    idFormaPago: {
        type: DataTypes.INTEGER,
    },
    idEstadoPago: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    fechaPago: {
        type: DataTypes.DATE,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    cantidad: {
        type: DataTypes.FLOAT,
    },
    archivo: {
        type: DataTypes.STRING,
    },
    datos: {
        type: DataTypes.JSON,
    },
}, {
    timestamps: false,
    schema: 'operaciones',
    tableName: 'pagos',
    sequelize
});

export default pagos;
