import {DataTypes, Model} from "sequelize";
import db from "../../db/connection";

const sequelize = db;
class CatalogoErrores extends Model{}

CatalogoErrores.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    idCampoCorregir: {
        type: DataTypes.BIGINT,
    },
    idTiempoResolucion: {
        type: DataTypes.INTEGER,
    },
    penalizacionEjecutivo: {
        type: DataTypes.FLOAT,
    },
    penalizacionSupervisor: {
        type: DataTypes.FLOAT,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.INTEGER,
    },
}, {
    timestamps: false,
    schema: 'generales',
    tableName: 'catalogoErrores',
    sequelize
});

export default CatalogoErrores;
