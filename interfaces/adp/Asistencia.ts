
export interface Asistencia {
    id?: number;
    idEmpleado: number;
    idEmpleadoSupervisor?: number;
    idEstadoAsistencia: number;
    fechaRegistro?: Date;
    solicitud: boolean;
    editable: number;
}
