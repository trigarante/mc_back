import { Sequelize } from 'sequelize';

const db = new Sequelize("mysql://servici1:rdsAdmin*tiv2@trigarante2020-1.cypym5wo7yt3.us-east-1.rds.amazonaws.com:3306/trigarante2020", {
    timezone: '-06:00'
});

/** PRODUCCIÓN **/
export const dbPostgres = new Sequelize(
    'produccion',
    'crm',
    'TstQ%#A2gMcn',
    {
        host: 'trigarante2021.cypym5wo7yt3.us-east-1.rds.amazonaws.com',
        dialect: 'postgres',
    }
);

/** PRUEBAS **/
// export const dbPostgres = new Sequelize(
//     'produccion',
//     'servici1',
//     'pruebasAdmin*tiv2',
//     {
//         host: 'ec2-3-239-90-172.compute-1.amazonaws.com',
//         dialect: 'postgres'
//     }
// );

export default db;
