import MotivoEndosoModel from "../../entity/motivoEndoso/motivoEndosoModel";

export default class MotivoEndosoQueries {

    static async findAllByIdTipoEndoso(idTipoEndoso): Promise<any> {
        return await MotivoEndosoModel.findAll({
            where: {idTipoEndoso}
        })
    }

}
