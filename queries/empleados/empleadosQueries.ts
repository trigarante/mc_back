import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class EmpleadosQueries {


    // Queries de empleados desarrollo organizacional

    static async getCorreoPersonal(idEmpleado){
        return await dbPostgres.query(`
            SELECT p.email
            FROM "recursosHumanos".empleado e
                JOIN "recursosHumanos".candidato c on e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato p on c."idPrecandidato" = p.id
            WHERE
            e.id = ${idEmpleado}
            `,{
            type: QueryTypes.SELECT,
            plain: true
        })

    }

    // FIN

}
