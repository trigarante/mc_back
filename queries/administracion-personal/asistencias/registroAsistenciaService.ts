import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import RegistroAsistenciaModel from "../../../models/administracion-personal/asistencias/registroAsistenciaModel";

export default class RegistroAsistenciaService {
    static async getByIdEmpleadoNow(idEmpleado) {
        return await dbPostgres.query(`
        SELECT
            "registroAsistencia".id id,
            "registroAsistencia"."idEmpleado" "idEmpleado",
            "registroAsistencia"."fechaRegistro" "fechaRegistro",
            "registroAsistencia"."idEmpleadoSupervisor" "idEmpleadoSupervisor",
            asistencias."idEstadoAsistencia" "idEstadoAsistencia"
        FROM
            "recursosHumanos"."registroAsistencia"
            left join "recursosHumanos".asistencias on "registroAsistencia"."idAsistencia"=asistencias.id
        WHERE
            "registroAsistencia"."idEmpleado" = ${idEmpleado} AND (DATE("registroAsistencia"."fechaRegistro") = DATE(now()))
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async post(data) {
        return await RegistroAsistenciaModel.create(data);
    }

    static async postAsistencia(data) {
        return await RegistroAsistenciaModel.create(data);
    }
    static async registrosDeHoy(idEmpleado) {
        return await dbPostgres.query(`
        SELECT
            *
        FROM
            "recursosHumanos"."registroAsistencia"
        WHERE
            "idEmpleadoSupervisor" = ${idEmpleado} AND (DATE("registroAsistencia"."fechaRegistro") = DATE(now()))
        `, {
            type: QueryTypes.SELECT,
            // plain: true
        })
    }
}
