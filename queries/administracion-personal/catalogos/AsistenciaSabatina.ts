
import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import AsistenciaSabatinaModel from "../../../models/administracion-personal/catalogos/AsistenciaSabatinaModel";

export default class AsistenciaSabatinaService {
    static async get() {
        return (await dbPostgres.query(`
            SELECT
                depa.descripcion AS departamento,
                ar.nombre AS area,
                asisa.activo as activo,
                asisa.id as id
                FROM "recursosHumanos"."asistenciaSabado" asisa
                LEFT JOIN "recursosHumanos".departamento depa on asisa."idDepartamento" = depa.id
                LEFT JOIN "recursosHumanos".area ar ON asisa."idArea" = ar.id
                ORDER BY asisa.id DESC
        `, {
            type: QueryTypes.SELECT
        }));
    }

    static async create(data: AsistenciaSabatinaModel) {
        return await AsistenciaSabatinaModel.create({...data})
    }

    static async update(idPuesto: number, data: AsistenciaSabatinaModel) {
        return await AsistenciaSabatinaModel.update({...data},
            {
                where: {
                    id: idPuesto
                }
            })
    }
}
