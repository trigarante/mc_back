import db from "../../db/connection";
import {QueryTypes} from "sequelize";

export default class PermisosQueries {
    static async findPermisos(idEmpleado): Promise<any> {
        return await db.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario",
            pl."idPuesto",
            pl.id
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async getPlazasHijo(id) {
        return await db.query(`
        SELECT empleado.id
        FROM "recursosHumanos".empleado
        JOIN "recursosHumanos".plaza ON plaza.id = empleado."idPlaza"
        WHERE plaza."idPadre" = ${id}`, {
            type: QueryTypes.SELECT

        })
    }
}
