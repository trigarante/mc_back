import db from "../../db/connection";
import {QueryTypes} from "sequelize";

export default class UsuariosQuery {
    static async getDepartamentoByIdUsuario(idUsuario): Promise<any> {
        return await db.query(`
            SELECT ud."idDepartamento"
            FROM generales."usuariosDepartamento" ud
            WHERE ud."idUsuario" = ${idUsuario}
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }
}
