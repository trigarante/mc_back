import ProductoClienteModel from "../../entity/productoCliente/productoClienteModel";

export default class ProductoClienteQueries {
    static async getById(id) {
        return await ProductoClienteModel.findOne({
            where: {id}
        })
    }

}
