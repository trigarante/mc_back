import db from "../../db/connection";
import {QueryTypes} from "sequelize";

export default class AplicacionesViewQueries {
    static async findAll(fechaInicio, fechaFin): Promise<any> {
        return await db.query(`
            SELECT
                r.id,
                r.poliza,
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                cl.correo,
                preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
                so."nombreComercial",
                dp.descripcion departamento,
                ep.estado AS "estadoPoliza",
                er."estadoDescripcion" AS "estadoRecibo",
                r."fechaInicio",
                re."fechaLiquidacion",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                re."idEstadoRecibos",
                case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
                espa.id AS "idEstadoP"
                FROM
                operaciones.registro r
                INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
                INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
                INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
                INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
                INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
                LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
                LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
                LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            WHERE
            re.activo = 1
            and
            re.numero = 1
            --and
            --r."idEstadoPoliza" <> 9
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT
        })
    }

    static async findAllByDepartamentos(fechaInicio, fechaFin, aux): Promise<any> {
        return await db.query(`
            SELECT
                r.id,
                r.poliza,
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                cl.correo,
                preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
                so."nombreComercial",
                dp.descripcion departamento,
                ep.estado AS "estadoPoliza",
                er."estadoDescripcion" AS "estadoRecibo",
                r."fechaInicio",
                re."fechaLiquidacion",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                re."idEstadoRecibos",
                case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago"
            FROM
                operaciones.registro r
                INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
                INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
                INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
                INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
                INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
                LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
                LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
                LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            WHERE
                r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            --AND
                --r."idDepartamento" IN (${aux})
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByHijos(fechaInicio, fechaFin, aux): Promise<any> {
        return await db.query(`
            SELECT
                r.id,
                r.poliza,
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                cl.correo,
                preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
                so."nombreComercial",
                dp.descripcion departamento,
                ep.estado AS "estadoPoliza",
                er."estadoDescripcion" AS "estadoRecibo",
                r."fechaInicio",
                re."fechaLiquidacion",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                re."idEstadoRecibos",
                case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago"
            FROM
                operaciones.registro r
                INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
                INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
                INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
                INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
                INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
                LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
                LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
                LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            WHERE
                r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
                r."idEmpleado" IN (${aux})
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async findAllByDepartamentoAndIdEmpleado(fechaInicio, fechaFin, idDepartamento, idEmpleado): Promise<any> {
        return await db.query(`
            SELECT
                r.id,
                r.poliza,
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                cl.correo,
                preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
                so."nombreComercial",
                dp.descripcion departamento,
                ep.estado AS "estadoPoliza",
                er."estadoDescripcion" AS "estadoRecibo",
                r."fechaInicio",
                re."fechaLiquidacion",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                re."idEstadoRecibos",
                case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago"
                -- de aqui para abajo segun yo no se usa
                /*
                r."primaNeta",
                r."fechaRegistro",
                r.archivo,
                tp."cantidadPagos",
                tp."tipoPago",
                ps.nombre AS "productoSocio",
                r."idEstadoPoliza",
                pz."idDepartamento",
                pc.datos,
                ep.estado AS "estadoPoliza",
                cl."telefonoMovil",
                case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
                re.id AS "idRecibo",
                CASE WHEN pc.archivo IS NOT NULL THEN pc.archivo ELSE cl.archivo END as "carpetaCliente",
                pa.id AS "idPago",
                pa."idEstadoPago",
                pc."idSolicitud",
                r.emitida,
                cl.id as "idCliente"
                */
            FROM
                operaciones.registro r
                INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
                INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
                INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
                INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
                INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
                LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
                LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
                LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            WHERE
                r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                --AND
                  --  r."idDepartamento" IN (${idDepartamento})
                AND r."idEmpleado" = ${idEmpleado}
                ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT
        })
    }

    static async findByidRegistro(idRegistro): Promise<any> {
        return await db.query(`
            SELECT
                r.id,
                r.poliza,
                r."fechaInicio",
                r."primaNeta",
                r."fechaRegistro",
                r.archivo,
                tp."cantidadPagos",
                tp."tipoPago",
                ps.nombre AS "productoSocio",
                r."idEstadoPoliza",
                ep.estado AS "estadoPoliza",
                preca.nombre,
                preca."apellidoPaterno",
                preca."apellidoMaterno",
                pz."idDepartamento",
                pc.datos,
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                so."nombreComercial",
                dp.descripcion,
                ep.estado AS "estadoPoliza",
                er."estadoDescripcion" AS "estadoRecibo",
                case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
                cl."telefonoMovil",
                r."fechaInicio",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
                re.id AS "idRecibo",
                CASE WHEN pc.archivo IS NOT NULL THEN pc.archivo ELSE cl.archivo END as "carpetaCliente",
                re."idEstadoRecibos",
                pa.id AS "idPago",
                pa."idEstadoPago",
                pc."idSolicitud",
                r.emitida,
                cl.id as "idCliente"
            FROM
                operaciones.registro r
                INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
                INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
                INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
                INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
                INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
                LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
                LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
                LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
                WHERE
                r.id = ${idRegistro}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        })
    }

    static async descargaPNC(fechaInicio, fechaFinal) {
        return await db.query(`
           SELECT
             DISTINCT ON ("operaciones"."erroresVerificacion".id,"operaciones"."erroresAnexosV".id,"operaciones"."verificacionRegistro".id)
              "operaciones".pagos."fechaPago"  AS fechaPago,
                "operaciones".registro."fechaInicio" AS fechaInicio,
                "operaciones"."formaPago".descripcion  AS metodoPago,
              "operaciones"."verificacionRegistro"."fechaCreacion"  AS fechaCreacionVerificacion,
              "operaciones".recibos."fechaLiquidacion" AS fechaAplicacionRecibo,
              "operaciones".recibos.numero AS seriePago,
              "operaciones".pagos.cantidad AS montoPago,
                "operaciones".recibos.cantidad AS montoRecibo,
              "operaciones".registro."primaNeta" AS montoRegitro,
              "operaciones"."tipoPago"."tipoPago" AS tipoPago,
              "operaciones".socios."nombreComercial" AS aseguradora,
              "operaciones".registro.poliza AS poliza,
                "operaciones"."flujoPoliza".estado AS flujoPoliza,
              "operaciones"."productoCliente"."noSerie"  AS numeroSerieVehiculo,
                "operaciones"."productoCliente".datos ->> 'numeroMotor' AS numeroMotor,
                "operaciones"."productoCliente".datos ->> 'numeroPlacas'  AS numeroPlacas,
              "operaciones".cliente.nombre || ' ' || cliente.paterno || ' ' || cliente.materno  AS nombreCliente,
              "operaciones".cliente."telefonoFijo"  AS telefonoFijo,
              "operaciones".cliente."telefonoMovil" AS telefonoMovil,
              "operaciones".cliente.correo AS correo,
                "operaciones".cliente.rfc AS rfc,
                "operaciones".cliente.cp  AS cp,
                "operaciones".cliente.calle  AS calle,
                "operaciones".cliente."numInt" AS numInt,
                "operaciones".cliente."numExt"  AS numExt,
                "operaciones"."cpSepomex".asenta AS colonia,
              "recursosHumanos".precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" AS nombreEjecutivo,
              "recursosHumanos".area.nombre AS areaEjecutivo,
              UPPER("recursosHumanos".sede.nombre)  AS sede,
              "operaciones"."productoCliente".datos ->> 'marca'  AS marca,
              "operaciones"."productoCliente".datos ->> 'descripcion'  AS descripcion,
              "operaciones"."productoCliente".datos ->> 'modelo'  AS anio,
              operaciones."productosSocio".nombre  AS cobertura,
              "operaciones"."tipoSubRamo".tipo  AS tipo,
              "recursosHumanos".departamento.descripcion AS departamento,
              operaciones."estadoRecibo"."estadoDescripcion"  AS estadoFinanzas,
              "operaciones"."estadoPoliza".estado  AS estadoPoliza,
              "operaciones"."estadoPago".descripcion    AS estadoPago,
                preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" AS "nombreSupervisor",
              "operaciones"."autorizacionRegistro"."fechaCreacion" AS fechaCreacionAutorizacion,
              "operaciones"."autorizacionRegistro"."fechaInicio"  AS fechaInicioAutorizacion,
              "operaciones"."autorizacionRegistro"."fechaConclusion"  AS fechaConclusionAutorizacion,
              "operaciones"."verificacionRegistro".id AS idVerificacion,
              "operaciones"."verificacionRegistro"."fechaInicio"  AS fechaInicioVerificacion,
              "operaciones"."verificacionRegistro"."fechaConclusion"  AS fechaConclusionVerificacion,
              "operaciones"."erroresAnexosV"."fechaCreacion" AS fechaCreacionAnexoV,
                "operaciones"."erroresAnexosV"."fechaCorreccion"  AS fechaCorrecionEjecutivoAnexosV,
              "operaciones"."erroresVerificacion"."fechaCreacion" AS fechaErroresVerificacion,
              "operaciones"."erroresVerificacion"."fechaCorreccion" AS fechaCorrecionEjecutivoVerificacion,
              "operaciones"."estadoVerificacion".estado AS estadoVerificacion,
                case "operaciones"."verificacionRegistro"."verificadoCliente"
                         WHEN  1  THEN 'Sin Verificar'
                     WHEN  2  THEN 'Espera correccion anexo'
                     WHEN  3  THEN 'Corregido'
                     WHEN  4  THEN 'Corrección terminada'
                     WHEN  5  THEN 'Espera corrección datos'
                     END AS verificacionCliente,

                case "operaciones"."verificacionRegistro"."verificadoRegistro"
                         WHEN 1 THEN 'Sin Verificar'
                     WHEN 2 THEN 'Espera corrección anexo'
                     WHEN 3 THEN 'Corregido'
                     WHEN 4 THEN 'Corrección terminada'
                     WHEN 5 THEN 'Espera corrección datos'
                     END AS verificacionRegistro,

                case "operaciones"."verificacionRegistro"."verificadoProducto"
                         WHEN 1 THEN 'Sin Verificar'
                     WHEN 2 THEN 'Espera corrección anexo'
                     WHEN 3 THEN 'Corregido'
                     WHEN 4 THEN 'Corrección terminada'
                     WHEN 5 THEN 'Espera corrección datos'
                     END AS verificacionProducto,

                case "operaciones"."verificacionRegistro"."verificadoPago"
                         WHEN 1 THEN 'Sin Verificar'
                     WHEN 2 THEN 'Espera corrección anexo'
                     WHEN 3 THEN 'Corregido'
                     WHEN 4 THEN 'Corrección terminada'
                     WHEN 5 THEN 'Espera corrección datos'
                     END AS verificacionPago,

             "operaciones"."erroresVerificacion".correcciones -> 0 ->>'campo' AS erroresVerificacionCampo,
             "operaciones"."erroresVerificacion".correcciones -> 0 ->>'idCampo' AS erroresVerificacionIdCampo,
             "operaciones"."erroresVerificacion".correcciones -> 0 ->>'comentario' AS erroresVerificacionComentario,
             "operaciones"."erroresVerificacion".correcciones -> 0 ->>'idCatalogoErrores' AS erroresVerificacionIdCatalogoErrores,
             "operaciones"."erroresAnexosV".correcciones -> 0 ->>'idTipoDocumento' AS erroresAnexoVIdTipoDocumento,
             "operaciones"."erroresAnexosV".correcciones -> 0 ->>'nombreDocumentoCorregir' AS erroresAnexoVNombreDocumentoCorregir,
             "operaciones"."erroresAnexosV".correcciones -> 0 ->>'comentario' AS erroresAnexoVComentario,


                prec.nombre || ' ' || prec."apellidoPaterno" || ' ' || prec."apellidoMaterno" AS "nombreAnalista",
                CASE WHEN operaciones.registro.online=1 THEN 'EMITIDA ONLINE' ELSE 'EMITIDA PORTAL' END AS tipoEmision,
                CASE WHEN operaciones."productoCliente"."idEmision" IS NULL THEN 'REFERIDO' ELSE 'RENOVACION' END AS descripcionEmision,
                CASE WHEN operaciones.registro."emisionCotizador"=1 THEN 'EMITIDA COTIZADOR' ELSE 'EMITIDA TRIGARANTE' END AS tipoCotizador


            FROM operaciones.registro
            LEFT JOIN "operaciones"."verificacionRegistro" ON("operaciones".registro.id="operaciones"."verificacionRegistro"."idRegistro")
            LEFT JOIN "operaciones".recibos ON("operaciones".registro.id="operaciones".recibos."idRegistro")
            LEFT JOIN "operaciones".pagos  ON("operaciones".recibos.id="operaciones".pagos."idRecibo")
            LEFT JOIN "operaciones"."erroresVerificacion" ON("operaciones"."verificacionRegistro".id="operaciones"."erroresVerificacion"."idVerificacionRegistro")
            LEFT JOIN "operaciones"."estadoPoliza" ON("operaciones".registro."idEstadoPoliza" = "operaciones"."estadoPoliza".id)
            LEFT JOIN "operaciones"."erroresAnexosV" ON("operaciones"."verificacionRegistro".id="operaciones"."erroresAnexosV"."idVerificacionRegistro")
            LEFT JOIN "recursosHumanos".empleado empErrores ON("operaciones"."erroresAnexosV"."idEmpleado"=empErrores.id)
            LEFT JOIN "operaciones"."estadoVerificacion" ON("operaciones"."verificacionRegistro"."idEstadoVerificacion"="operaciones"."estadoVerificacion".id)
            LEFT JOIN operaciones."productosSocio" ON("operaciones".registro."idProductoSocio" = operaciones."productosSocio"."id")
            LEFT JOIN "operaciones"."subRamo" ON(operaciones."productosSocio"."idSubRamo" = "operaciones"."subRamo".id)
            LEFT JOIN "operaciones"."tipoSubRamo" ON("operaciones"."subRamo"."idTipoSubRamo" = "operaciones"."tipoSubRamo".id)
            LEFT JOIN "operaciones".ramo ON("operaciones"."subRamo"."idRamo"="operaciones".ramo.id)
            LEFT JOIN "operaciones".socios ON("operaciones".ramo."idSocio" = "operaciones".socios.id)
            LEFT JOIN"operaciones"."productoCliente" ON("operaciones".registro."idProducto"="operaciones"."productoCliente".id)
            LEFT JOIN "operaciones"."estadoRecibo" ON("operaciones".recibos."idEstadoRecibos"="operaciones"."estadoRecibo".id)
            LEFT JOIN "operaciones"."estadoPago" ON("operaciones".pagos."idEstadoPago"="operaciones"."estadoPago".id)
            LEFT JOIN "operaciones"."flujoPoliza" ON("operaciones".registro."idFlujoPoliza"="operaciones"."flujoPoliza".id)
            LEFT JOIN "operaciones"."tipoPago" ON("operaciones".registro."idTipoPago"="operaciones"."tipoPago".id)
            LEFT JOIN "operaciones".cliente  ON("operaciones"."productoCliente"."idCliente"="operaciones".cliente.id)
            LEFT JOIN "recursosHumanos".empleado emp ON("operaciones".registro."idEmpleado"=emp.id)
            LEFT JOIN "recursosHumanos".candidato ON(emp."idCandidato"="recursosHumanos".candidato.id)
            LEFT JOIN "recursosHumanos".precandidato ON("recursosHumanos".candidato."idPrecandidato"="recursosHumanos".precandidato.id)
            LEFT JOIN "recursosHumanos"."plaza" ON(emp."idPlaza"="recursosHumanos"."plaza".id)
            LEFT JOIN "recursosHumanos".departamento ON ("recursosHumanos".plaza."idDepartamento"="recursosHumanos".departamento.id)
            LEFT JOIN "recursosHumanos".area ON("recursosHumanos"."plaza"."idArea"="recursosHumanos".area.id)
            LEFT JOIN "recursosHumanos"."sede" ON ("recursosHumanos".plaza."idSede"="recursosHumanos".sede.id)
            LEFT JOIN "operaciones"."autorizacionRegistro" ON("operaciones".registro.id="operaciones"."autorizacionRegistro"."idRegistro")
            LEFT JOIN "operaciones"."formaPago" ON ("operaciones".pagos."idFormaPago"="operaciones"."formaPago".id)
            LEFT JOIN "operaciones"."cpSepomex" ON ("operaciones".cliente."idColonia"="cpSepomex"."idCp")
            LEFT JOIN "recursosHumanos".empleado empl on (operaciones."verificacionRegistro"."idEmpleado"=empl."id")
            LEFT JOIN "recursosHumanos".candidato cand on (empl."idCandidato"=cand."id")
            LEFT JOIN "recursosHumanos".precandidato prec on (cand."idPrecandidato"=prec."id")
            LEFT JOIN "recursosHumanos".empleado emple on (operaciones."autorizacionRegistro"."idEmpleado"=emple."id")
            LEFT JOIN "recursosHumanos".candidato candi on (emple."idCandidato"=candi."id")
            LEFT JOIN "recursosHumanos".precandidato preca on (candi."idPrecandidato"=preca."id")


            WHERE DATE("operaciones".pagos."fechaPago") BETWEEN '${fechaInicio}' AND '${fechaFinal}'
            AND  "operaciones".recibos.activo=1
            AND "operaciones".recibos.numero = 1
            AND "operaciones".registro."idEmpleado" NOT IN (2993)
        `, {
            type: QueryTypes.SELECT
        });
    }
    /**
     * query para  filtar polizas por poliza, serie o nombre en el admin de polizas
     * @param query recibe filtro dinamico acorde al tipo de filtro seleccionado
     */
    static async getDatos(query): Promise<any> {
        return await db.query(`
           SELECT
                r.id,
                r.poliza,
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno as "nombreCliente",
                cl.correo,
                preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
                so."nombreComercial",
                dp.descripcion departamento,
                ep.estado AS "estadoPoliza",
                er."estadoDescripcion" as "estadoRecibo",
                r."fechaInicio",
                re."fechaLiquidacion",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END as "numeroSerie",
                re."idEstadoRecibos",
                case when pa.id is not null then espa.descripcion else 'SIN PAGO' end as "estadoPago",
                espa.id AS "idEstadoP"
            FROM
                operaciones.registro r
                INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
                INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
                INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
                INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
                INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
                LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
                LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
                LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            WHERE
            re.activo = 1
            and
            re.numero = 1
            ${query}
        `, {
            type: QueryTypes.SELECT
        });
    }


}
