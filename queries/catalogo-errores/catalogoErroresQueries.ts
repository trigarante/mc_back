import db from "../../db/connection";
import {QueryTypes} from "sequelize";

export default class CatalogoErroresQueries {
    static async getCatalogoErrores(): Promise<any> {
        return await db.query(`
            SELECT
                generales."catalogoErrores".id,
                generales."catalogoErrores"."idCampoCorregir",
                generales."catalogoErrores"."idTiempoResolucion",
                generales."catalogoErrores"."penalizacionEjecutivo",
                generales."catalogoErrores"."penalizacionSupervisor",
                generales."catalogoErrores".descripcion,
                generales."catalogoErrores".activo,
                operaciones."campoCorregir".descripcion "campo",
                operaciones."campoCorregir"."idTabla",
                generales."tiempoResolucion"."horasResolucion"
            FROM generales."catalogoErrores"
                INNER JOIN operaciones."campoCorregir" ON operaciones."campoCorregir".id = generales."catalogoErrores"."idCampoCorregir"
                INNER JOIN generales."tiempoResolucion" ON generales."tiempoResolucion".id = generales."catalogoErrores"."idTiempoResolucion"
            ORDER BY generales."catalogoErrores".id DESC
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async getCatalogoErroresById(id): Promise<any> {
        return await db.query(`
            SELECT
                generales."catalogoErrores".id,
                generales."catalogoErrores"."idCampoCorregir",
                generales."catalogoErrores"."idTiempoResolucion",
                generales."catalogoErrores"."penalizacionEjecutivo",
                generales."catalogoErrores"."penalizacionSupervisor",
                generales."catalogoErrores".descripcion,
                generales."catalogoErrores".activo,
                operaciones."campoCorregir".descripcion "campo",
                operaciones."campoCorregir"."idTabla",
                generales."tiempoResolucion"."horasResolucion"
            FROM generales."catalogoErrores"
                INNER JOIN operaciones."campoCorregir" ON operaciones."campoCorregir".id = generales."catalogoErrores"."idCampoCorregir"
                INNER JOIN generales."tiempoResolucion" ON generales."tiempoResolucion".id = generales."catalogoErrores"."idTiempoResolucion"
            WHERE generales."catalogoErrores".id = ${id}
            ORDER BY generales."catalogoErrores".id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }
}
