import SolicitudEndosoModel from "../../entity/solicitudEndoso/solicitudEndosoModel";
import {QueryTypes} from "sequelize";
import db from "../../db/connection";

export default class SolicitudEndosoQueries {
    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await db.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario"
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
    // solicitudEndoso
    static async getAll(fechaInicio, fechaFin,): Promise<any> {
        return await db.query(`
           SELECT
                se.id,
                r.poliza,
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" AS "Empleado",
                "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "EmpleadoSolicitante",
                "precandidatoEmisor".nombre || ' ' || "precandidatoEmisor"."apellidoPaterno" || ' ' || "precandidatoEmisor"."apellidoMaterno" AS "emisorPoliza",
                esen.descripcion AS "estadoEndoso",
                esol.estado AS "estado",
                dp.descripcion AS departamento,
                se."fechaSolicitud"
                FROM generales."solicitudEndoso" se
                JOIN generales."estadoEndoso" esen ON se."idEstadoEndoso" = esen.id
                JOIN operaciones.registro r ON se."idRegistro" = r.id
                JOIN "recursosHumanos".empleado e ON se."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoSolicitante" ON se."idEmpleado" = "empleadoSolicitante".id
                JOIN "recursosHumanos".candidato "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante".id
                JOIN "recursosHumanos".precandidato "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante".id
                JOIN "recursosHumanos".empleado "empleadoEmisor" ON r."idEmpleado" = "empleadoEmisor".id
                JOIN "recursosHumanos".candidato "candidatoEmisor" ON "empleadoEmisor"."idCandidato" = "candidatoEmisor".id
                JOIN "recursosHumanos".precandidato "precandidatoEmisor" ON "candidatoEmisor"."idPrecandidato" = "precandidatoEmisor".id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones."estadoSolicitud" esol ON dp.id = esol."idDepartamento"
                WHERE
                se."fechaSolicitud" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                ORDER by se.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
    // solicitudEndoso
    static async getAllByDepartamentos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await db.query(`
             SELECT
                se.id,
                r.poliza,
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" AS "Empleado",
                "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "EmpleadoSolicitante",
                "precandidatoEmisor".nombre || ' ' || "precandidatoEmisor"."apellidoPaterno" || ' ' || "precandidatoEmisor"."apellidoMaterno" AS "emisorPoliza",
                esen.descripcion AS "estadoEndoso",
                esol.estado AS "estado",
                dp.descripcion AS departamento,
                se."fechaSolicitud"
                FROM generales."solicitudEndoso" se
                JOIN generales."estadoEndoso" esen ON se."idEstadoEndoso" = esen.id
                JOIN operaciones.registro r ON se."idRegistro" = r.id
                JOIN "recursosHumanos".empleado e ON se."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoSolicitante" ON se."idEmpleado" = "empleadoSolicitante".id
                JOIN "recursosHumanos".candidato "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante".id
                JOIN "recursosHumanos".precandidato "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante".id
                JOIN "recursosHumanos".empleado "empleadoEmisor" ON r."idEmpleado" = "empleadoEmisor".id
                JOIN "recursosHumanos".candidato "candidatoEmisor" ON "empleadoEmisor"."idCandidato" = "candidatoEmisor".id
                JOIN "recursosHumanos".precandidato "precandidatoEmisor" ON "candidatoEmisor"."idPrecandidato" = "precandidatoEmisor".id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones."estadoSolicitud" esol ON dp.id = esol."idDepartamento"
                WHERE
                se."fechaSolicitud" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by se.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByHijos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await db.query(`
             SELECT
                se.id,
                r.poliza,
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" AS "Empleado",
                "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "EmpleadoSolicitante",
                "precandidatoEmisor".nombre || ' ' || "precandidatoEmisor"."apellidoPaterno" || ' ' || "precandidatoEmisor"."apellidoMaterno" AS "emisorPoliza",
                esen.descripcion AS "estadoEndoso",
                esol.estado AS "estado",
                dp.descripcion AS departamento,
                se."fechaSolicitud"
                FROM generales."solicitudEndoso" se
                JOIN generales."estadoEndoso" esen ON se."idEstadoEndoso" = esen.id
                JOIN operaciones.registro r ON se."idRegistro" = r.id
                JOIN "recursosHumanos".empleado e ON se."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoSolicitante" ON se."idEmpleado" = "empleadoSolicitante".id
                JOIN "recursosHumanos".candidato "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante".id
                JOIN "recursosHumanos".precandidato "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante".id
                JOIN "recursosHumanos".empleado "empleadoEmisor" ON r."idEmpleado" = "empleadoEmisor".id
                JOIN "recursosHumanos".candidato "candidatoEmisor" ON "empleadoEmisor"."idCandidato" = "candidatoEmisor".id
                JOIN "recursosHumanos".precandidato "precandidatoEmisor" ON "candidatoEmisor"."idPrecandidato" = "precandidatoEmisor".id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones."estadoSolicitud" esol ON dp.id = esol."idDepartamento"
                WHERE
                se."fechaSolicitud" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                AND
                r."idEmpleado" IN (${departamentos})
                ORDER by se.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // solicitudEndoso
    static async getAllByDepartamentoAndIdEmpleado(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await db.query(`
            SELECT
                se.id,
                r.poliza,
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" AS "Empleado",
                "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "EmpleadoSolicitante",
                "precandidatoEmisor".nombre || ' ' || "precandidatoEmisor"."apellidoPaterno" || ' ' || "precandidatoEmisor"."apellidoMaterno" AS "emisorPoliza",
                esen.descripcion AS "estadoEndoso",
                esol.estado AS "estado",
                dp.descripcion AS departamento,
                se."fechaSolicitud"
                FROM generales."solicitudEndoso" se
                JOIN generales."estadoEndoso" esen ON se."idEstadoEndoso" = esen.id
                JOIN operaciones.registro r ON se."idRegistro" = r.id
                JOIN "recursosHumanos".empleado e ON se."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoSolicitante" ON se."idEmpleado" = "empleadoSolicitante".id
                JOIN "recursosHumanos".candidato "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante".id
                JOIN "recursosHumanos".precandidato "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante".id
                JOIN "recursosHumanos".empleado "empleadoEmisor" ON r."idEmpleado" = "empleadoEmisor".id
                JOIN "recursosHumanos".candidato "candidatoEmisor" ON "empleadoEmisor"."idCandidato" = "candidatoEmisor".id
                JOIN "recursosHumanos".precandidato "precandidatoEmisor" ON "candidatoEmisor"."idPrecandidato" = "precandidatoEmisor".id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones."estadoSolicitud" esol ON dp.id = esol."idDepartamento"
                WHERE
                se."fechaSolicitud" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                -- AND
                -- dp.id IN (${idDepartamento})
                AND
                se."idEmpleado" = ${idEmpleado}
                ORDER by se.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async post(data){
        return await SolicitudEndosoModel.create(data).then(data => {
            return data['id'];
        })
    }
}
