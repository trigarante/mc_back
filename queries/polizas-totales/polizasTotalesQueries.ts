import {DataTypes, QueryTypes} from 'sequelize';
import db from "../../db/connection";

export default class Polizas {
    static async findAllPolizas(): Promise<any> {
        return await db.query(`
              SELECT
        operaciones.registro."id",
        operaciones.registro."idEmpleado",
        operaciones.registro."idProducto",
        operaciones.registro."idTipoPago",
        operaciones.registro."idProductoSocio",
        operaciones.registro."idFlujoPoliza",
        operaciones.registro.poliza,
        operaciones.registro."fechaInicio",
        operaciones.registro."primaNeta",
        operaciones.registro."fechaRegistro",
        operaciones.registro.archivo,
        operaciones."tipoPago"."tipoPago" "tipoPago",
        operaciones."productosSocio".nombre "productoSocio",
        operaciones."productoCliente".datos "datos",
        operaciones.registro."idEstadoPoliza" "idEstadoPoliza",
        operaciones."estadoPoliza".estado,
        "recursosHumanos".departamento."idArea" "idArea",
        "recursosHumanos".precandidato.nombre,
        "recursosHumanos".precandidato."apellidoPaterno" "apellidoPaterno",
        "recursosHumanos".precandidato."apellidoMaterno" "apellidoMaterno",
        "recursosHumanos".plaza."idSede",
        "recursosHumanos".empleado."idPuesto",
        "recursosHumanos".sede."idEmpresa",
        "recursosHumanos".empleado."idDepartamento" "idSubarea",
        operaciones."productoCliente".datos->> 'descripcion' "descripcion",
        CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
        "recursosHumanos".departamento.descripcion "subarea",
        operaciones.cliente."idPais",
        operaciones."productoCliente"."idCliente",
        operaciones."productoCliente"."idSolicitud",
        operaciones."productoCliente"."idSubRamo",
        operaciones.cliente.nombre "nombreCliente",
        operaciones.cliente.paterno "apellidoPaternoCliente",
        operaciones.cliente.materno "apellidoMaternoCliente",
        operaciones.cliente.correo,
        operaciones.cliente."telefonoMovil",
        CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
        operaciones.recibos."id" "idRecibo",
        operaciones.recibos."idEstadoRecibos" "idEstadoRecibo",
        operaciones.recibos."fechaLiquidacion",
        operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
        operaciones.recibos."fechaPromesaPago",
        operaciones.cliente."archivoSubido",
        operaciones.socios."nombreComercial",
        operaciones."tipoSubRamo".tipo "tipoSubRamo",
        operaciones.registro."idSocio"
        FROM
        operaciones.registro
        INNER JOIN operaciones."tipoPago" on operaciones."tipoPago"."id" = operaciones.registro."idTipoPago"
        INNER JOIN operaciones."productoCliente" on operaciones."productoCliente"."id" = operaciones.registro."idProducto"
        INNER JOIN operaciones.cliente on operaciones.cliente."id" = operaciones."productoCliente"."id"
        INNER JOIN "recursosHumanos".empleado on "recursosHumanos".empleado."id" = operaciones.registro."idEmpleado"
        INNER JOIN "recursosHumanos".candidato on "recursosHumanos".candidato."id" = "recursosHumanos".empleado."idCandidato"
        INNER JOIN "recursosHumanos".precandidato on "recursosHumanos".precandidato."id" = "recursosHumanos".candidato."idPrecandidato"
        INNER JOIN operaciones.socios on operaciones.socios."id" = operaciones.registro."idSocio"
        INNER JOIN operaciones."estadoPoliza" on operaciones."estadoPoliza"."id" = operaciones.registro."idEstadoPoliza"
        INNER JOIN operaciones.recibos on operaciones.recibos."idRegistro" = operaciones.registro."id"
        INNER JOIN operaciones."estadoRecibo" on operaciones."estadoRecibo"."id" = operaciones.recibos."idEstadoRecibos"
        INNER JOIN operaciones."productosSocio" on operaciones."productosSocio"."id" = operaciones.registro."idSocio"
        INNER JOIN operaciones.usuarios on generales.usuarios."id" = "recursosHumanos".empleado."idUsuario"
        INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento."id" = "recursosHumanos".empleado."idDepartamento"
        INNER JOIN "recursosHumanos".area on "recursosHumanos".area."id" = "recursosHumanos".departamento."idArea"
        INNER JOIN "recursosHumanos".plaza on "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento."id"
        INNER JOIN "recursosHumanos".sede on "recursosHumanos".sede."id" = "recursosHumanos".plaza."idSede"
        INNER JOIN  operaciones."tipoSubRamo" on operaciones."tipoSubRamo"."id" = operaciones."productosSocio"."idSubRamo"
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }
}
