import db from "../../db/connection";
import {QueryTypes} from "sequelize";

export default class EmpleadoQueries {
    static async findIdPuestoByIdEmpleado(idEmpleado): Promise<any> {
        return await db.query(`
            SELECT 
                "recursosHumanos".empleado."idPuesto",
                "recursosHumanos".empleado."idDepartamento"
            FROM "recursosHumanos".empleado
            WHERE id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }
}
