import db from "../../db/connection";
import {QueryTypes} from "sequelize";

export default class CatalogoErroresDocsQueries {
    static async findAllCatalogo(idCatalogoErrores): Promise<any> {
        return await db.query(`
            SELECT
                generales."catalogoErroresDocumentos".id,
                generales."catalogoErroresDocumentos"."idTipoDocumento",
                generales."catalogoErroresDocumentos"."idTiempoResolucion",
                generales."catalogoErroresDocumentos"."penalizacionEjecutivo",
                generales."catalogoErroresDocumentos"."penalizacionSupervisor",
                generales."catalogoErroresDocumentos".descripcion,
                generales."catalogoErroresDocumentos".activo,
                operaciones."tipoDocumentos"."idTabla",
                operaciones."tipoDocumentos".descripcion "tipoDocumento",
                generales."tiempoResolucion"."horasResolucion"
            FROM generales."catalogoErroresDocumentos"
                INNER JOIN operaciones."tipoDocumentos" ON operaciones."tipoDocumentos".id = generales."catalogoErroresDocumentos"."idTipoDocumento"
                INNER JOIN generales."tiempoResolucion" ON generales."tiempoResolucion".id = generales."catalogoErroresDocumentos"."idTiempoResolucion"
            WHERE generales."catalogoErroresDocumentos".id = ${idCatalogoErrores}
            `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }
}
