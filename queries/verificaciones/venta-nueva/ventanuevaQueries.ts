import db from "../../../db/connection";
import {QueryTypes} from "sequelize";
import VerificacionRegistro from "../../../entity/verificaciones/verificacionRegistroEntity";

export default class VentanuevaQueries {
    static async findAllIdVarificacionRegistro(idVerificacionRegistro): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionRegistro".id,
                operaciones."verificacionRegistro"."idRegistro",
                operaciones."verificacionRegistro"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado,
                operaciones."verificacionRegistro".numero,
                operaciones."verificacionRegistro"."fechaCreacion",
                operaciones."verificacionRegistro"."verificadoCliente",
                operaciones."verificacionRegistro"."verificadoProducto",
                operaciones."verificacionRegistro"."verificadoRegistro",
                operaciones."verificacionRegistro"."verificadoPago",
                operaciones."verificacionRegistro"."verificadoInspeccion",
                operaciones."verificacionRegistro"."idEmpleado",
                "precandidatoMesa".nombre "nombreEmpleadoMesa",
                "precandidatoMesa"."apellidoPaterno" "apellidoPaternoEmpleadoMesa",
                "precandidatoMesa"."apellidoMaterno" "apellidoMaternoEmpleadoMesa",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio",
                operaciones.registro."idEmpleado" "idEmpleadoRegistro",
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                operaciones."cliente".id "idCliente",
                operaciones."cliente".nombre "nombreCliente",
                operaciones."cliente".paterno "apellidoMaternoCliente",
                operaciones."cliente".materno "apellidoMaternoCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente"."idPais",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."pagos".id "idPago",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."idEstadoPago",
                operaciones."pagos"."fechaPago",
                operaciones."tipoPago"."cantidadPagos",
                operaciones.socios."nombreComercial",
                operaciones."estadoPoliza".estado "estadoPoliza",
                operaciones."estadoPago".descripcion "estadoPago",
                operaciones.inspecciones.archivo "archivoInspeccion",
                operaciones.inspecciones.id "idInspeccion",
                operaciones."recibos"."fechaLiquidacion",
                operaciones."verificacionRegistro"."fechaConclusion",
                operaciones.solicitudes.id "idSolicitud",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
            FROM operaciones."verificacionRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."verificacionRegistro"."idEstadoVerificacion" = operaciones."estadoVerificacion".id
                INNER JOIN "recursosHumanos".empleado "empleadoMesa" ON "empleadoMesa".id = operaciones."verificacionRegistro"."idEmpleado"
                INNER JOIN "recursosHumanos".candidato "candidatoMesa" ON "candidatoMesa".id = "empleadoMesa"."idCandidato"
                INNER JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "precandidatoMesa".id = "candidatoMesa"."idPrecandidato"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "empleadoMesa"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "operaciones".registro."idDepartamento"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = "operaciones"."registro"."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones."recibos" ON operaciones."recibos"."idRegistro" = operaciones.registro.id
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."pagos" ON operaciones."pagos"."idRecibo" = operaciones."recibos".id
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones."pagos"."idEstadoPago"
                INNER JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
                INNER JOIN operaciones."productosSocio" ON operaciones."productosSocio".id = operaciones.registro."idProductoSocio"
                INNER JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productosSocio"."idSubRamo"
                INNER JOIN operaciones."ramo" ON operaciones."ramo".id = operaciones."subRamo"."idRamo"
                INNER JOIN operaciones."socios" ON operaciones."socios".id = operaciones."ramo"."idSocio"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones."registro"."idEstadoPoliza"
                INNER JOIN operaciones.inspecciones ON operaciones.inspecciones."idRegistro" = operaciones."verificacionRegistro"."idRegistro"
                INNER JOIN operaciones.solicitudes ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
            WHERE operaciones."verificacionRegistro".id = ${idVerificacionRegistro}
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAll(fechaInicio, fechaFin, idFlujoPoliza, aux2, aux3, aux4): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionRegistro".id,
                operaciones."verificacionRegistro"."idRegistro",
                operaciones."verificacionRegistro"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado,
                operaciones."verificacionRegistro".numero,
                operaciones."verificacionRegistro"."fechaCreacion",
                operaciones."verificacionRegistro"."verificadoCliente",
                operaciones."verificacionRegistro"."verificadoProducto",
                operaciones."verificacionRegistro"."verificadoRegistro",
                operaciones."verificacionRegistro"."verificadoPago",
                operaciones."verificacionRegistro"."verificadoInspeccion",
                operaciones."verificacionRegistro"."idEmpleado",
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' ' ||  "precandidatoMesa"."apellidoMaterno" "nombreEmpleadoMesa",
                operaciones."cliente".nombre || ' ' || operaciones."cliente".paterno || ' ' || operaciones."cliente".materno "nombreCliente",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio",
                operaciones.registro."idEmpleado" "idEmpleadoRegistro",
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                operaciones.registro."idTipificacionDesviacion",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                operaciones."cliente".id "idCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente"."idPais",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."pagos".id "idPago",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."idEstadoPago",
                operaciones."pagos"."fechaPago",
                operaciones."tipoPago"."cantidadPagos",
                operaciones.socios."nombreComercial",
                operaciones."estadoPoliza".estado "estadoPoliza",
                operaciones."estadoPago".descripcion "estadoPago",
                ${aux4}
                operaciones."recibos"."fechaLiquidacion",
                operaciones."verificacionRegistro"."fechaConclusion",
                operaciones.solicitudes.id "idSolicitud",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                CASE WHEN operaciones.registro.online = 1 THEN 'EMITIDA ONLINE' ELSE 'EMITIDA PORTAL' END AS "tipoEmision",
                CASE WHEN operaciones."productoCliente"."idEmision" IS NULL THEN 'REFERIDO' ELSE 'RENOVACION' END AS "descripcionEmision",
                CASE WHEN operaciones.registro."emisionCotizador" = 1 THEN 'EMITIDA COTIZADOR' ELSE 'EMITIDA TRIGARANTE' END AS "tipoCotizador"
                FROM operaciones."verificacionRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."verificacionRegistro"."idEstadoVerificacion" = operaciones."estadoVerificacion".id
                LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON "empleadoMesa".id = operaciones."verificacionRegistro"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "candidatoMesa".id = "empleadoMesa"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "precandidatoMesa".id = "candidatoMesa"."idPrecandidato"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "empleadoMesa"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "operaciones".registro."idDepartamento"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = "operaciones"."registro"."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones."recibos" ON operaciones."recibos"."idRegistro" = operaciones.registro.id
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."pagos" ON operaciones."pagos"."idRecibo" = operaciones."recibos".id
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones."pagos"."idEstadoPago"
                INNER JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
                INNER JOIN operaciones."productosSocio" ON operaciones."productosSocio".id = operaciones.registro."idProductoSocio"
                INNER JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productosSocio"."idSubRamo"
                INNER JOIN operaciones."ramo" ON operaciones."ramo".id = operaciones."subRamo"."idRamo"
                INNER JOIN operaciones."socios" ON operaciones."socios".id = operaciones."ramo"."idSocio"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones."registro"."idEstadoPoliza"
                ${aux3}
                LEFT JOIN operaciones.solicitudes ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
                WHERE
                recibos.numero =1 AND recibos.activo = 1
                and operaciones.registro."idEstadoPoliza" <> 9
                and operaciones.registro."idEstadoPoliza" <> 27
                AND recibos."fechaCierre" IS NULL AND
                "verificacionRegistro"."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}') AND
                registro."idFlujoPoliza" = ${idFlujoPoliza} AND ${aux2}
                ORDER BY "verificacionRegistro".id DESC
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAllByDepartamentos(fechaInicio, fechaFin, idFlujoPoliza, deptos, aux2, aux3, aux4): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionRegistro".id,
                operaciones."verificacionRegistro"."idRegistro",
                operaciones."verificacionRegistro"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado,
                operaciones."verificacionRegistro".numero,
                operaciones."verificacionRegistro"."fechaCreacion",
                operaciones."verificacionRegistro"."verificadoCliente",
                operaciones."verificacionRegistro"."verificadoProducto",
                operaciones."verificacionRegistro"."verificadoRegistro",
                operaciones."verificacionRegistro"."verificadoPago",
                operaciones."verificacionRegistro"."verificadoInspeccion",
                operaciones."verificacionRegistro"."idEmpleado",
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' ' ||  "precandidatoMesa"."apellidoMaterno" "nombreEmpleadoMesa",
                operaciones."cliente".nombre || ' ' || operaciones."cliente".paterno || ' ' || operaciones."cliente".materno "nombreCliente",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio",
                operaciones.registro."idEmpleado" "idEmpleadoRegistro",
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                operaciones.registro."idTipificacionDesviacion",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                operaciones."cliente".id "idCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente"."idPais",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."pagos".id "idPago",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."idEstadoPago",
                operaciones."pagos"."fechaPago",
                operaciones."tipoPago"."cantidadPagos",
                operaciones.socios."nombreComercial",
                operaciones."estadoPoliza".estado "estadoPoliza",
                operaciones."estadoPago".descripcion "estadoPago",
                ${aux4}
                operaciones."recibos"."fechaLiquidacion",
                operaciones."verificacionRegistro"."fechaConclusion",
                operaciones.solicitudes.id "idSolicitud",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
            FROM operaciones."verificacionRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."verificacionRegistro"."idEstadoVerificacion" = operaciones."estadoVerificacion".id
                LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON "empleadoMesa".id = operaciones."verificacionRegistro"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "candidatoMesa".id = "empleadoMesa"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "precandidatoMesa".id = "candidatoMesa"."idPrecandidato"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "empleadoMesa"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "operaciones".registro."idDepartamento"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = "operaciones"."registro"."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones."recibos" ON operaciones."recibos"."idRegistro" = operaciones.registro.id
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."pagos" ON operaciones."pagos"."idRecibo" = operaciones."recibos".id
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones."pagos"."idEstadoPago"
                INNER JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
                INNER JOIN operaciones."productosSocio" ON operaciones."productosSocio".id = operaciones.registro."idProductoSocio"
                INNER JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productosSocio"."idSubRamo"
                INNER JOIN operaciones."ramo" ON operaciones."ramo".id = operaciones."subRamo"."idRamo"
                INNER JOIN operaciones."socios" ON operaciones."socios".id = operaciones."ramo"."idSocio"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones."registro"."idEstadoPoliza"
                ${aux3}
                INNER JOIN operaciones.solicitudes ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
                WHERE
                recibos.numero =1 AND recibos.activo = 1
                and operaciones.registro."idEstadoPoliza" <> 9
                and operaciones.registro."idEstadoPoliza" <> 27
                AND recibos."fechaCierre" IS NULL AND
                "verificacionRegistro"."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}') AND
                registro."idFlujoPoliza" = ${idFlujoPoliza}
                -- AND registro."idDepartamento" IN (${deptos})
                AND ${aux2}
                ORDER BY "verificacionRegistro".id DESC
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAllByHijos(fechaInicio, fechaFin, idFlujoPoliza, deptos, aux2, aux3, aux4): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionRegistro".id,
                operaciones."verificacionRegistro"."idRegistro",
                operaciones."verificacionRegistro"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado,
                operaciones."verificacionRegistro".numero,
                operaciones."verificacionRegistro"."fechaCreacion",
                operaciones."verificacionRegistro"."verificadoCliente",
                operaciones."verificacionRegistro"."verificadoProducto",
                operaciones."verificacionRegistro"."verificadoRegistro",
                operaciones."verificacionRegistro"."verificadoPago",
                operaciones."verificacionRegistro"."verificadoInspeccion",
                operaciones."verificacionRegistro"."idEmpleado",
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' ' ||  "precandidatoMesa"."apellidoMaterno" "nombreEmpleadoMesa",
                operaciones."cliente".nombre || ' ' || operaciones."cliente".paterno || ' ' || operaciones."cliente".materno "nombreCliente",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio",
                operaciones.registro."idEmpleado" "idEmpleadoRegistro",
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                operaciones."cliente".id "idCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente"."idPais",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."pagos".id "idPago",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."idEstadoPago",
                operaciones."pagos"."fechaPago",
                operaciones."tipoPago"."cantidadPagos",
                operaciones.socios."nombreComercial",
                operaciones."estadoPoliza".estado "estadoPoliza",
                operaciones."estadoPago".descripcion "estadoPago",
                ${aux4}
                operaciones."recibos"."fechaLiquidacion",
                operaciones."verificacionRegistro"."fechaConclusion",
                operaciones.solicitudes.id "idSolicitud",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
            FROM operaciones."verificacionRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."verificacionRegistro"."idEstadoVerificacion" = operaciones."estadoVerificacion".id
                LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON "empleadoMesa".id = operaciones."verificacionRegistro"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "candidatoMesa".id = "empleadoMesa"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "precandidatoMesa".id = "candidatoMesa"."idPrecandidato"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "empleadoMesa"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "operaciones".registro."idDepartamento"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = "operaciones"."registro"."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones."recibos" ON operaciones."recibos"."idRegistro" = operaciones.registro.id
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."pagos" ON operaciones."pagos"."idRecibo" = operaciones."recibos".id
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones."pagos"."idEstadoPago"
                INNER JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
                INNER JOIN operaciones."productosSocio" ON operaciones."productosSocio".id = operaciones.registro."idProductoSocio"
                INNER JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productosSocio"."idSubRamo"
                INNER JOIN operaciones."ramo" ON operaciones."ramo".id = operaciones."subRamo"."idRamo"
                INNER JOIN operaciones."socios" ON operaciones."socios".id = operaciones."ramo"."idSocio"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones."registro"."idEstadoPoliza"
                ${aux3}
                LEFT JOIN operaciones.solicitudes ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
                WHERE
                recibos.numero =1 AND recibos.activo = 1 AND registro."idEstadoPoliza" <> 9 AND recibos."fechaCierre" IS NULL AND
                "verificacionRegistro"."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}') AND
                registro."idFlujoPoliza" = ${idFlujoPoliza} AND
                registro."idEmpleado" IN (${deptos})
                AND ${aux2}
                ORDER BY "verificacionRegistro".id DESC
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAllByDepartamentoAndIdEmpleado(fechaInicio, fechaFin, idFlujoPoliza, idDepartamento, idEmpleado, aux2): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionRegistro".id,
                operaciones."verificacionRegistro"."idRegistro",
                operaciones."verificacionRegistro"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado,
                operaciones."verificacionRegistro".numero,
                operaciones."verificacionRegistro"."fechaCreacion",
                operaciones."verificacionRegistro"."verificadoCliente",
                operaciones."verificacionRegistro"."verificadoProducto",
                operaciones."verificacionRegistro"."verificadoRegistro",
                operaciones."verificacionRegistro"."verificadoPago",
                operaciones."verificacionRegistro"."verificadoInspeccion",
                operaciones."verificacionRegistro"."idEmpleado",
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' ' ||  "precandidatoMesa"."apellidoMaterno" "nombreEmpleadoMesa",
                operaciones."cliente".nombre || ' ' || operaciones."cliente".paterno || ' ' || operaciones."cliente".materno "nombreCliente",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio",
                operaciones.registro."idEmpleado" "idEmpleadoRegistro",
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                operaciones.registro."idTipificacionDesviacion",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                operaciones."cliente".id "idCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente"."idPais",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."pagos".id "idPago",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."idEstadoPago",
                operaciones."pagos"."fechaPago",
                operaciones."tipoPago"."cantidadPagos",
                operaciones.socios."nombreComercial",
                operaciones."estadoPoliza".estado "estadoPoliza",
                operaciones."estadoPago".descripcion "estadoPago",
                operaciones."recibos"."fechaLiquidacion",
                operaciones."verificacionRegistro"."fechaConclusion",
                operaciones.solicitudes.id "idSolicitud",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
            FROM operaciones."verificacionRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."verificacionRegistro"."idEstadoVerificacion" = operaciones."estadoVerificacion".id
                LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON "empleadoMesa".id = operaciones."verificacionRegistro"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "candidatoMesa".id = "empleadoMesa"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "precandidatoMesa".id = "candidatoMesa"."idPrecandidato"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "empleadoMesa"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "operaciones".registro."idDepartamento"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = "operaciones"."registro"."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones."recibos" ON operaciones."recibos"."idRegistro" = operaciones.registro.id
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."pagos" ON operaciones."pagos"."idRecibo" = operaciones."recibos".id
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones."pagos"."idEstadoPago"
                INNER JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
                INNER JOIN operaciones."productosSocio" ON operaciones."productosSocio".id = operaciones.registro."idProductoSocio"
                INNER JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productosSocio"."idSubRamo"
                INNER JOIN operaciones."ramo" ON operaciones."ramo".id = operaciones."subRamo"."idRamo"
                INNER JOIN operaciones."socios" ON operaciones."socios".id = operaciones."ramo"."idSocio"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones."registro"."idEstadoPoliza"
                LEFT JOIN operaciones.inspecciones ON operaciones.inspecciones."idRegistro" = operaciones."verificacionRegistro"."idRegistro"
                LEFT JOIN operaciones.solicitudes ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
                WHERE
                recibos.numero = 1 AND recibos.activo = 1
                and operaciones.registro."idEstadoPoliza" <> 9
                and operaciones.registro."idEstadoPoliza" <> 27
                AND recibos."fechaCierre" IS NULL AND
                "verificacionRegistro"."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}') AND
                registro."idFlujoPoliza" = ${idFlujoPoliza}
                -- AND registro."idDepartamento" = ${idDepartamento}
                AND ${aux2}
                --AND operaciones."verificacionRegistro"."idEmpleado" = ${idEmpleado}
                ORDER BY "verificacionRegistro".id DESC
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async update(data, id) {
        return await VerificacionRegistro.update(data, {
            where: {id},
        })
    }

    static async getById(id): Promise<any> {
        return await db.query(`
            SELECT
            DISTINCT ON (r.id)
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ar."idEmpleado" AS "idEmpleadoAutorizacion",
                r."idEmpleado",
                r."idProducto",
                r.poliza,
                pc."idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                CASE WHEN pc.archivo IS NOT NULL THEN pc.archivo ELSE cl.archivo END as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                i.archivo AS "archivoInspeccion",
                r."idFlujoPoliza" AS "flujoPoliza",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                pg.id AS "idPago",
                pg.archivo AS "archivoPago",
                pg."fechaPago"
                FROM
                operaciones."verificacionRegistro" ar
                INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                INNER JOIN operaciones.socios s ON r."idSocio" = s.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
            WHERE ar.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }


// busqueda por poliza

    static async findAllPoliza(poliza, idFlujoPoliza, aux2, aux3, aux4): Promise<any> {
        return await db.query(`
SELECT
operaciones."verificacionRegistro".id,
operaciones."verificacionRegistro"."idRegistro",
operaciones."verificacionRegistro"."idEstadoVerificacion",
operaciones."estadoVerificacion".estado,
operaciones."verificacionRegistro".numero,
operaciones."verificacionRegistro"."fechaCreacion",
operaciones."verificacionRegistro"."verificadoCliente",
operaciones."verificacionRegistro"."verificadoProducto",
operaciones."verificacionRegistro"."verificadoRegistro",
operaciones."verificacionRegistro"."verificadoPago",
operaciones."verificacionRegistro"."verificadoInspeccion",
operaciones."verificacionRegistro"."idEmpleado",
"precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' ' ||  "precandidatoMesa"."apellidoMaterno" "nombreEmpleadoMesa",
operaciones."cliente".nombre || ' ' || operaciones."cliente".paterno || ' ' || operaciones."cliente".materno "nombreCliente",
operaciones.registro."idProducto",
operaciones.registro.poliza,
operaciones.registro."primaNeta",
operaciones.registro."fechaInicio",
operaciones.registro."idEmpleado" "idEmpleadoRegistro",
operaciones.registro.archivo "carpetaRegistro",
operaciones.registro."idFlujoPoliza",
"recursosHumanos".departamento.id "idDepartamento",
"recursosHumanos".departamento.descripcion "descripcionDepartamento",
operaciones."cliente".id "idCliente",
CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
operaciones."cliente"."idPais",
operaciones."cliente".correo,
operaciones."cliente"."telefonoMovil",
"recursosHumanos".precandidato.nombre,
"recursosHumanos".precandidato."apellidoPaterno",
"recursosHumanos".precandidato."apellidoMaterno",
operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
operaciones."pagos".id "idPago",
operaciones."pagos".archivo "archivoPago",
operaciones."pagos"."idEstadoPago",
${aux4}
operaciones."pagos"."fechaPago",
operaciones."tipoPago"."cantidadPagos",
operaciones.socios."nombreComercial",
operaciones."estadoPoliza".estado "estadoPoliza",
operaciones."estadoPago".descripcion "estadoPago",
operaciones."recibos"."fechaLiquidacion",
operaciones."verificacionRegistro"."fechaConclusion",
operaciones.solicitudes.id "idSolicitud",
CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
CASE WHEN operaciones.registro.online = 1 THEN 'EMITIDA ONLINE' ELSE 'EMITIDA PORTAL' END AS "tipoEmision",
CASE WHEN operaciones."productoCliente"."idEmision" IS NULL THEN 'REFERIDO' ELSE 'RENOVACION' END AS "descripcionEmision",
CASE WHEN operaciones.registro."emisionCotizador" = 1 THEN 'EMITIDA COTIZADOR' ELSE 'EMITIDA TRIGARANTE' END AS "tipoCotizador"
FROM operaciones."verificacionRegistro"
LEFT JOIN operaciones."estadoVerificacion" ON operaciones."verificacionRegistro"."idEstadoVerificacion" = operaciones."estadoVerificacion".id
LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON "empleadoMesa".id = operaciones."verificacionRegistro"."idEmpleado"
LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "candidatoMesa".id = "empleadoMesa"."idCandidato"
LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "precandidatoMesa".id = "candidatoMesa"."idPrecandidato"
FULL OUTER JOIN operaciones.registro ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "empleadoMesa"."idPlaza"
LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "operaciones".registro."idDepartamento"
LEFT JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
LEFT JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = "operaciones"."registro"."idEmpleado"
LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
LEFT JOIN operaciones."recibos" ON operaciones."recibos"."idRegistro" = operaciones.registro.id
LEFT JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
LEFT JOIN operaciones."pagos" ON operaciones."pagos"."idRecibo" = operaciones."recibos".id
LEFT JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones."pagos"."idEstadoPago"
LEFT JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
LEFT JOIN operaciones."productosSocio" ON operaciones."productosSocio".id = operaciones.registro."idProductoSocio"
LEFT JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productosSocio"."idSubRamo"
LEFT JOIN operaciones."ramo" ON operaciones."ramo".id = operaciones."subRamo"."idRamo"
LEFT JOIN operaciones."socios" ON operaciones."socios".id = operaciones."ramo"."idSocio"
LEFT JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones."registro"."idEstadoPoliza"
${aux3}
LEFT JOIN operaciones.solicitudes ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
WHERE
recibos.numero =1
AND recibos.activo = 1
and operaciones.registro."idEstadoPoliza" <> 9
and operaciones.registro."idEstadoPoliza" <> 27
AND recibos."fechaCierre" IS NULL
AND
operaciones.registro.poliza = '${poliza}'
AND
registro."idFlujoPoliza" = '${idFlujoPoliza}'
AND
${aux2}

        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }


    // buscar por numero de serie

    static async getAllBySerie(numero, idFlujoPoliza, aux2, aux3, aux4): Promise<any> {
        return await db.query(`
            SELECT
operaciones."verificacionRegistro".id,
operaciones."verificacionRegistro"."idRegistro",
operaciones."verificacionRegistro"."idEstadoVerificacion",
operaciones."estadoVerificacion".estado,
operaciones."verificacionRegistro".numero,
operaciones."verificacionRegistro"."fechaCreacion",
operaciones."verificacionRegistro"."verificadoCliente",
operaciones."verificacionRegistro"."verificadoProducto",
operaciones."verificacionRegistro"."verificadoRegistro",
operaciones."verificacionRegistro"."verificadoPago",
operaciones."verificacionRegistro"."verificadoInspeccion",
operaciones."verificacionRegistro"."idEmpleado",
"precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' ' ||  "precandidatoMesa"."apellidoMaterno" "nombreEmpleadoMesa",
operaciones."cliente".nombre || ' ' || operaciones."cliente".paterno || ' ' || operaciones."cliente".materno "nombreCliente",
operaciones.registro."idProducto",
operaciones.registro.poliza,
operaciones.registro."primaNeta",
operaciones.registro."fechaInicio",
operaciones.registro."idEmpleado" "idEmpleadoRegistro",
operaciones.registro.archivo "carpetaRegistro",
operaciones.registro."idFlujoPoliza",
"recursosHumanos".departamento.id "idDepartamento",
"recursosHumanos".departamento.descripcion "descripcionDepartamento",
operaciones."cliente".id "idCliente",
CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
operaciones."cliente"."idPais",
operaciones."cliente".correo,
operaciones."cliente"."telefonoMovil",
"recursosHumanos".precandidato.nombre,
"recursosHumanos".precandidato."apellidoPaterno",
"recursosHumanos".precandidato."apellidoMaterno",
operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
operaciones."pagos".id "idPago",
operaciones."pagos".archivo "archivoPago",
operaciones."pagos"."idEstadoPago",
operaciones."pagos"."fechaPago",
operaciones."tipoPago"."cantidadPagos",
operaciones.socios."nombreComercial",
operaciones."estadoPoliza".estado "estadoPoliza",
operaciones."estadoPago".descripcion "estadoPago",
${aux4}
operaciones."recibos"."fechaLiquidacion",
operaciones."verificacionRegistro"."fechaConclusion",
operaciones.solicitudes.id "idSolicitud",
CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
CASE WHEN operaciones.registro.online = 1 THEN 'EMITIDA ONLINE' ELSE 'EMITIDA PORTAL' END AS "tipoEmision",
CASE WHEN operaciones."productoCliente"."idEmision" IS NULL THEN 'REFERIDO' ELSE 'RENOVACION' END AS "descripcionEmision",
CASE WHEN operaciones.registro."emisionCotizador" = 1 THEN 'EMITIDA COTIZADOR' ELSE 'EMITIDA TRIGARANTE' END AS "tipoCotizador"
FROM operaciones."verificacionRegistro"
LEFT JOIN operaciones."estadoVerificacion" ON operaciones."verificacionRegistro"."idEstadoVerificacion" = operaciones."estadoVerificacion".id
LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON "empleadoMesa".id = operaciones."verificacionRegistro"."idEmpleado"
LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "candidatoMesa".id = "empleadoMesa"."idCandidato"
LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "precandidatoMesa".id = "candidatoMesa"."idPrecandidato"
FULL OUTER JOIN operaciones.registro ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "empleadoMesa"."idPlaza"
LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "operaciones".registro."idDepartamento"
FULL OUTER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
LEFT JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = "operaciones"."registro"."idEmpleado"
LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
LEFT JOIN operaciones."recibos" ON operaciones."recibos"."idRegistro" = operaciones.registro.id
LEFT JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
LEFT JOIN operaciones."pagos" ON operaciones."pagos"."idRecibo" = operaciones."recibos".id
LEFT JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones."pagos"."idEstadoPago"
LEFT JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
LEFT JOIN operaciones."productosSocio" ON operaciones."productosSocio".id = operaciones.registro."idProductoSocio"
LEFT JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productosSocio"."idSubRamo"
LEFT JOIN operaciones."ramo" ON operaciones."ramo".id = operaciones."subRamo"."idRamo"
LEFT JOIN operaciones."socios" ON operaciones."socios".id = operaciones."ramo"."idSocio"
LEFT JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones."registro"."idEstadoPoliza"
${aux3}
LEFT JOIN operaciones.solicitudes ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
WHERE
recibos.numero =1
AND recibos.activo = 1
and operaciones.registro."idEstadoPoliza" <> 9
and operaciones.registro."idEstadoPoliza" <> 27
AND recibos."fechaCierre" IS NULL
AND
operaciones."productoCliente"."noSerie" = '${numero}'
AND
registro."idFlujoPoliza" = '${idFlujoPoliza}'
AND
${aux2}
        `, {
            type: QueryTypes.SELECT,
        })
    }



}

