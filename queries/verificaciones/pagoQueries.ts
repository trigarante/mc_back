import db from "../../db/connection";
import {QueryTypes} from "sequelize";
import PagoEntity from "../../entity/pagos/pagoEntity";

export default class PagoQueries {
    static async findAllPago(fechaInicio, fechaFin, aux2): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionPago".id,
                operaciones."verificacionPago"."idPago",
                operaciones."verificacionPago"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado "estadoVerificacion",
                operaciones."verificacionPago"."verificadoPago",
                operaciones."verificacionPago"."fechaCreacion",
                operaciones."verificacionPago"."idEmpleado",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio" "inicioVigencia",
                operaciones."cliente".id "idCliente",
                operaciones."cliente".nombre "nombreCliente",
                operaciones."cliente".paterno "apellidoPaternoCliente",
                operaciones."cliente".materno "apellidoMaternoCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."fechaPago",
                operaciones.recibos.numero "numeroRecibo",
                operaciones."socios"."nombreComercial",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."estadoPago".descripcion "estadoPago",
                operaciones."estadoPoliza".estado "estadoPoliza",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
                FROM operaciones."verificacionPago"
                INNER JOIN operaciones.pagos ON operaciones.pagos.id = operaciones."verificacionPago"."idPago"
                INNER JOIN operaciones.recibos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones.recibos."idRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."estadoVerificacion".id = operaciones."verificacionPago"."idEstadoVerificacion"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones.pagos."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones.socios ON operaciones.socios.id = operaciones.registro."idSocio"
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones.pagos."idEstadoPago"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos".empleado."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones.registro."idEstadoPoliza"
            WHERE
                operaciones."verificacionPago"."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
                operaciones.recibos.numero > 1
            AND
                operaciones.registro.emitida = 1
            AND
                ${aux2}
            ORDER BY operaciones."verificacionPago".id DESC
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAllByIdDepartamento(fechaInicio, fechaFin, aux, aux2): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionPago".id,
                operaciones."verificacionPago"."idPago",
                operaciones."verificacionPago"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado "estadoVerificacion",
                operaciones."verificacionPago"."verificadoPago",
                operaciones."verificacionPago"."fechaCreacion",
                operaciones."verificacionPago"."idEmpleado",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio" "inicioVigencia",
                operaciones."cliente".id "idCliente",
                operaciones."cliente".nombre "nombreCliente",
                operaciones."cliente".paterno "apellidoPaternoCliente",
                operaciones."cliente".materno "apellidoMaternoCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."fechaPago",
                operaciones.recibos.numero "numeroRecibo",
                operaciones."socios"."nombreComercial",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."estadoPago".descripcion "estadoPago",
                operaciones."estadoPoliza".estado "estadoPoliza",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
                FROM operaciones."verificacionPago"
                INNER JOIN operaciones.pagos ON operaciones.pagos.id = operaciones."verificacionPago"."idPago"
                INNER JOIN operaciones.recibos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones.recibos."idRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."estadoVerificacion".id = operaciones."verificacionPago"."idEstadoVerificacion"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones.pagos."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones.socios ON operaciones.socios.id = operaciones.registro."idSocio"
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones.pagos."idEstadoPago"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos".empleado."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones.registro."idEstadoPoliza"
            WHERE
                operaciones."verificacionPago"."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
                operaciones.registro."idDepartamento" IN (${aux})
            AND
                ${aux2}
            ORDER BY operaciones."verificacionPago".id DESC
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAllByHijos(fechaInicio, fechaFin, aux, aux2): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionPago".id,
                operaciones."verificacionPago"."idPago",
                operaciones."verificacionPago"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado "estadoVerificacion",
                operaciones."verificacionPago"."verificadoPago",
                operaciones."verificacionPago"."fechaCreacion",
                operaciones."verificacionPago"."idEmpleado",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio" "inicioVigencia",
                operaciones."cliente".id "idCliente",
                operaciones."cliente".nombre "nombreCliente",
                operaciones."cliente".paterno "apellidoPaternoCliente",
                operaciones."cliente".materno "apellidoMaternoCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."fechaPago",
                operaciones.recibos.numero "numeroRecibo",
                operaciones."socios"."nombreComercial",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."estadoPago".descripcion "estadoPago",
                operaciones."estadoPoliza".estado "estadoPoliza",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
                FROM operaciones."verificacionPago"
                INNER JOIN operaciones.pagos ON operaciones.pagos.id = operaciones."verificacionPago"."idPago"
                INNER JOIN operaciones.recibos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones.recibos."idRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."estadoVerificacion".id = operaciones."verificacionPago"."idEstadoVerificacion"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones.pagos."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones.socios ON operaciones.socios.id = operaciones.registro."idSocio"
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones.pagos."idEstadoPago"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos".empleado."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones.registro."idEstadoPoliza"
            WHERE
                operaciones."verificacionPago"."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
                operaciones.registro."idEmpleado" IN (${aux})
            AND
                ${aux2}
            ORDER BY operaciones."verificacionPago".id DESC
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findByIdEmpledo(fechaInicio, fechaFin, idDepartamento, idEmpleado , aux2): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionPago".id,
                operaciones."verificacionPago"."idPago",
                operaciones."verificacionPago"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado "estadoVerificacion",
                operaciones."verificacionPago"."verificadoPago",
                operaciones."verificacionPago"."fechaCreacion",
                operaciones."verificacionPago"."idEmpleado",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio" "inicioVigencia",
                operaciones."cliente".id "idCliente",
                operaciones."cliente".nombre "nombreCliente",
                operaciones."cliente".paterno "apellidoPaternoCliente",
                operaciones."cliente".materno "apellidoMaternoCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."fechaPago",
                operaciones.recibos.numero "numeroRecibo",
                operaciones."socios"."nombreComercial",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."estadoPago".descripcion "estadoPago",
                operaciones."estadoPoliza".estado "estadoPoliza",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
                FROM operaciones."verificacionPago"
                INNER JOIN operaciones.pagos ON operaciones.pagos.id = operaciones."verificacionPago"."idPago"
                INNER JOIN operaciones.recibos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones.recibos."idRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."estadoVerificacion".id = operaciones."verificacionPago"."idEstadoVerificacion"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones.pagos."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones.socios ON operaciones.socios.id = operaciones.registro."idSocio"
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones.pagos."idEstadoPago"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos".empleado."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones.registro."idEstadoPoliza"
            WHERE
                operaciones."verificacionPago"."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            --AND
              --  operaciones.registro."idDepartamento" = ${idDepartamento}
            AND
                ${aux2}
            ORDER BY operaciones."verificacionPago".id DESC
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAllByid(idVerificacionPago): Promise<any> {
        return await db.query(`
            SELECT
                operaciones."verificacionPago".id,
                operaciones."verificacionPago"."idPago",
                operaciones."verificacionPago"."idEstadoVerificacion",
                operaciones."estadoVerificacion".estado "estadoVerificacion",
                operaciones."verificacionPago"."verificadoPago",
                operaciones."verificacionPago"."fechaCreacion",
                operaciones."verificacionPago"."idEmpleado",
                operaciones.registro."idProducto",
                operaciones.registro.poliza,
                operaciones.registro.archivo "carpetaRegistro",
                operaciones.registro."idFlujoPoliza",
                operaciones.registro."primaNeta",
                operaciones.registro."fechaInicio" "inicioVigencia",
                operaciones."cliente".id "idCliente",
                operaciones."cliente".nombre "nombreCliente",
                operaciones."cliente".paterno "apellidoPaternoCliente",
                operaciones."cliente".materno "apellidoMaternoCliente",
                CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
                operaciones."cliente".correo,
                operaciones."cliente"."telefonoMovil",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                "recursosHumanos".precandidato.nombre,
                "recursosHumanos".precandidato."apellidoPaterno",
                "recursosHumanos".precandidato."apellidoMaterno",
                operaciones."pagos".archivo "archivoPago",
                operaciones."pagos"."fechaPago",
                operaciones.recibos.numero "numeroRecibo",
                operaciones."socios"."nombreComercial",
                operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
                operaciones."estadoPago".descripcion "estadoPago",
                operaciones."estadoPoliza".estado "estadoPoliza",
                CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie"
                FROM operaciones."verificacionPago"
                INNER JOIN operaciones.pagos ON operaciones.pagos.id = operaciones."verificacionPago"."idPago"
                INNER JOIN operaciones.recibos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones.recibos."idRegistro"
                INNER JOIN operaciones."estadoVerificacion" ON operaciones."estadoVerificacion".id = operaciones."verificacionPago"."idEstadoVerificacion"
                INNER JOIN operaciones."productoCliente" ON operaciones."productoCliente".id = operaciones.registro."idProducto"
                INNER JOIN operaciones."cliente" ON operaciones."cliente".id = operaciones."productoCliente"."idCliente"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones.pagos."idEmpleado"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
                INNER JOIN operaciones.socios ON operaciones.socios.id = operaciones.registro."idSocio"
                INNER JOIN operaciones."estadoRecibo" ON operaciones."estadoRecibo".id = operaciones.recibos."idEstadoRecibos"
                INNER JOIN operaciones."estadoPago" ON operaciones."estadoPago".id = operaciones.pagos."idEstadoPago"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos".empleado."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                INNER JOIN operaciones."estadoPoliza" ON operaciones."estadoPoliza".id = operaciones.registro."idEstadoPoliza"
            WHERE operaciones."verificacionPago".id = ${idVerificacionPago}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findByIdVerificacion(idVerificacion): Promise<any> {
        return await db.query(`
            SELECT
                operaciones.pagos.id "idPago"
            FROM operaciones."verificacionRegistro"
                INNER JOIN operaciones.registro ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
                INNER JOIN operaciones.recibos ON operaciones.registro.id = operaciones.recibos."idRegistro"
                INNER JOIN operaciones.pagos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
            WHERE operaciones."verificacionRegistro".id = ${idVerificacion}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }
    static async updateEdoPago(data, id): Promise<any> {
        return await PagoEntity.update(data,{where: {id}});
    }
}
