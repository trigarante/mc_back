import {QueryTypes} from "sequelize";
import db from "../../db/connection";
import pagos from "../../entity/pagos/pagoEntity";

export default class PagosQueries {
    static async getById(id): Promise<any> {
        return await db.query(`
            SELECT
                    pa.id,
                    pa."idRecibo",
                    pa."idFormaPago",
                    pa."idEmpleado",
                    pa."idEstadoPago",
                    pa."fechaPago",
                    pa.cantidad,
                    pa.archivo,
                    pa.datos,
                    re.numero,
                    re.cantidad AS recibosCantidad,
                    fp.descripcion AS "formapagoDescripcio",
                    pa.datos::json ->>'numeroTarjeta' AS "numeroTarjeta",
                    pa.datos::json ->>'titular' AS titular
                    FROM operaciones.pagos pa
                    JOIN operaciones.recibos re ON pa."idRecibo" = re.id
                    JOIN operaciones."formaPago" fp ON pa."idFormaPago" = fp.id
                    JOIN operaciones."estadoPago" ep ON pa."idEstadoPago" = ep.id
            WHERE pa.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }
    static async update(data, id) {
        return await pagos.update(data, {
            where: {id}
        })
    }
    static async post(data){
        return await pagos.create(data).then(data => {
            return data['id'];
        })
    }
}
