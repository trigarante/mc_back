import TipoEndosoModel from "../../entity/tipoEndoso/tipoEndosoModel";

export default class TipoEndosoQueries {

    static async getAll(): Promise<any> {
        return await TipoEndosoModel.findAll({
            where: {activo: 1}
        })
    }

}
