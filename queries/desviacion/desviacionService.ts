import pagos from "../../entity/pagos/pagoEntity";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";
import LogsDesviadasModel from "../../entity/desviaciones/logsDesviadasModel";
import RegistroEntity from "../../entity/registro/registroEntity";

export default class DesviacionService {

    static async update(data, id) {
        return await pagos.update(data, {
            where: {id}
        })
    }

    static async deviacionByIdRegistro(idDesviacion) {
        return await dbPostgres.query(`
        SELECT
            "desviacionesPolizas".ID,
            "desviacionesPolizas"."idEmpleadoComercial",
            "desviacionesPolizas"."fechaInicio",
            "desviacionesPolizas"."idEstadoDesviacion",
            "desviacionesPolizas"."fechaFin",
            "desviacionesPolizas"."comentarios",
            "motivosDesviacionesPolizas".motivo,
            "estadoDesviacionPolizas".estado "estadoDesviacion",
            prComercial.nombre || ' ' || prComercial."apellidoPaterno" || ' ' || prComercial."apellidoMaterno" AS "empleadoComercial"
        FROM
            operaciones."desviacionesPolizas"
            LEFT JOIN "recursosHumanos".empleado eComercial ON "desviacionesPolizas"."idEmpleadoComercial" = eComercial.ID
            LEFT JOIN "recursosHumanos".candidato cComercial ON eComercial."idCandidato" = cComercial."id"
            LEFT JOIN "recursosHumanos".precandidato prComercial ON cComercial."idPrecandidato" = prComercial."id"
            INNER JOIN operaciones."motivosDesviacionesPolizas" ON "desviacionesPolizas"."idMotivoDesviacion" = "motivosDesviacionesPolizas".ID
            INNER JOIN operaciones."estadoDesviacionPolizas" ON "estadoDesviacionPolizas".ID = "desviacionesPolizas"."idEstadoDesviacion"

        WHERE
            "desviacionesPolizas".id = ${idDesviacion}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async departamentosAndSociosByIdDesviacion(id) {
        return await dbPostgres.query(`
        SELECT
            "desviacionesPolizas".ID,
            "array_agg" ( DISTINCT "departamentosDesviacionPoliza"."idDepartamento" ) AS "idDepartamento",
            "array_agg" ( DISTINCT "departamentosDesviacionPoliza"."idSocio" ) AS "idSocio",
            "string_agg" ( DISTINCT socios."nombreComercial", ', ' ) AS socio,
            "string_agg" ( DISTINCT departamento.descripcion, ', ' ) AS departamento
        FROM
            operaciones."desviacionesPolizas"
            INNER JOIN operaciones."departamentosDesviacionPoliza" ON "desviacionesPolizas".ID = "departamentosDesviacionPoliza"."idDesviacionPoliza"
            INNER JOIN "recursosHumanos".departamento ON "departamentosDesviacionPoliza"."idDepartamento" = departamento.ID
            AND "departamentosDesviacionPoliza".activo = 1
            INNER JOIN operaciones.socios ON "departamentosDesviacionPoliza"."idSocio" = socios.ID
        WHERE
            "desviacionesPolizas"."id" = ${id}
        GROUP BY
            "desviacionesPolizas".ID
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async postLog(data) {
        return await LogsDesviadasModel.create(data, {
        });
    }
    static async updateRegistro(data, id) {
        return await RegistroEntity.update(data, {
            where: {id}
        })
    }

    static async tipoDesviacion(id) {
        return await RegistroEntity.findOne({
            where: { id },
            raw: true
        })
    }
}
