import {QueryTypes} from "sequelize";
import db from "../../db/connection";

export default class RegistroQueries {
    static async getForViewer(id): Promise<any> {
        return await db.query(`
          SELECT
            DISTINCT ON (recibos.id)
            recibos.id AS "idRecibo",
            registro.id,
            registro.archivo AS "archivoPoliza",
            registro.online,
            CASE WHEN "productoCliente".archivo IS NOT NULL THEN "productoCliente".archivo ELSE "cliente".archivo END as "carpetaCliente",
            inspecciones.archivo AS "archivoInspeccion",
            pagos.archivo AS "archivoPago"
            FROM operaciones.registro
            LEFT JOIN operaciones."productoCliente"  ON registro."idProducto"="productoCliente".id
            LEFT JOIN operaciones.cliente  ON "productoCliente"."idCliente"=cliente.id
            LEFT JOIN operaciones.recibos  ON registro.id = recibos."idRegistro"
            LEFT JOIN operaciones.pagos  ON recibos.id = pagos."idRecibo"
            LEFT JOIN operaciones.inspecciones ON registro.id = inspecciones."idRegistro"
            WHERE registro.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }

    static async findAllByIdRegistro(id): Promise<any> {
        return await db.query(`
          SELECT
                DISTINCT ON (r.id)
                r.id,
                r."idEmpleado",
                r."idProducto",
                r."idTipoPago",
                r."idProductoSocio",
                r."idFlujoPoliza",
                fp.estado,
                r.poliza,
                TO_CHAR(r."fechaInicio", 'YYYY-MM-DD') as "fechaInicio",
                r.archivo,
                r."idPeriodicidad",
                r."idSocio",
                per.nombre AS periodicidad,
                tp."cantidadPagos",
                tp."tipoPago",
                "tipoProducto".tipo AS "productoSocio",
                ps."idSubRamo",
                tsr.tipo AS "tipoSubramo",
                sr."idRamo",
                pc.datos,
                case r."idEstadoPoliza" when 9 then 'cancelado' when 10 then 'Por corregir' else pc."noSerie" end AS "numSerie",
                r."idEstadoPoliza",
                ep.estado,
                dp."idArea",
                preca.nombre,
                preca."apellidoPaterno",
                preca."apellidoMaterno",
                pz."idSede",
                pz."idPuesto",
                sed."idEmpresa",
                dp.id AS "idDepartamento",
                dp.descripcion AS departamento,
                pc."idCliente",
                cl."archivoSubido",
                tr.tipo as "tipoRamo",
                so.alias,
                cl."idPais",
                r.oficina,
                pc."idSubRamo" AS "productoClienteSubRamo",
                pc."idSolicitud",
                r."primaNeta"
                FROM operaciones.registro r
                JOIN operaciones."tipoPago" tp ON r."idTipoPago" = tp.id
                JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps.id
                JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                JOIN operaciones."tipoSubRamo" tsr ON sr."idTipoSubRamo" = tsr.id
                JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                JOIN operaciones."tipoRamo" tr ON ra."idTipoRamo" = tr.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones."estadoPoliza" ep ON r."idEstadoPoliza" = ep.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato ca ON e."idCandidato" = ca.id
                JOIN "recursosHumanos".precandidato preca ON ca."idPrecandidato" = preca.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN operaciones."flujoPoliza" fp ON r."idFlujoPoliza" = fp.id
                JOIN operaciones.socios so ON ra."idSocio" = so.id
                JOIN operaciones.periodicidad per ON r."idPeriodicidad" = per.id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN "recursosHumanos".area ar ON dp."idArea" = ar.id
                JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz."id"
                JOIN "recursosHumanos".sede sed ON pz."idSede" = sed.id
                JOIN "recursosHumanos".empresas emp ON sed."idEmpresa" = emp.id
                JOIN operaciones."tipoProducto" ON "tipoProducto".id = ps."idTipoProducto"
                WHERE
                r.id = ${id}
                ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }
}

